/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

/** @class HCHitChecker HCHitChecker.h
 *
 *  Algorithm run in Gauss for monitoring Herschel MCHits.
 *
 */

class MCHit;

class HCHitChecker : public GaudiTupleAlg {
public:
  /// Standard constructor
  HCHitChecker(const std::string& name, ISvcLocator* pSvcLocator);
  /// Destructor
  virtual ~HCHitChecker();

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;       ///< Algorithm execution

private:

  /// TES location of MCHits
  std::string m_hitsLocation;

};
