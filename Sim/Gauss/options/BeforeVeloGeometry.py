###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Switch on geometry upstream of Velo
from Gaudi.Configuration import *
from Configurables import GiGaInputStream

geo = GiGaInputStream('Geo')
geo.StreamItems += [ "/dd/Structure/LHCb/UpstreamRegion/BlockWallUpstr" ]
geo.StreamItems += [ "/dd/Structure/LHCb/BeforeMagnetRegion/PipeJunctionBeforeVelo" ]
geo.StreamItems += [ "/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo" ]


