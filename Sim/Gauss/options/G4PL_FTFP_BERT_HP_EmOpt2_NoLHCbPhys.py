###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##
##  File containing options to activate the FTFP_BERT_HP Hadronic
##  Physics List in Geant4 to add low energy neutron processes with
##  the new Opt2 and switching off optical photon processes.

from Configurables import Gauss

Gauss().PhysicsList = { "Em":'Opt2', "Hadron":'FTFP_BERT_HP', "GeneralPhys":True, "LHCbPhys":False }
