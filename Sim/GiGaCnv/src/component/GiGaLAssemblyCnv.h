/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GIGACNV_GIGALASSEMBLYCNV_H 
#define GIGACNV_GIGALASSEMBLYCNV_H 1
// Include files
// GiGaCnv
#include "GiGaCnv/GiGaCnvBase.h"
#include "GiGaCnv/GiGaLeaf.h"


/** @class GiGaLAssemblyCnv GiGaLAssemblyCnv.h
 *
 *  The specific converter for LAssembly objects
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   19/01/2002
 */

class GiGaLAssemblyCnv: public GiGaCnvBase
{
  
public:
  
  /** Standard constructor
   *  @param Locator pointer to service locator
   */
  GiGaLAssemblyCnv( ISvcLocator* Locator );

  /** destructor
   */
  virtual ~GiGaLAssemblyCnv();

public:
    /** create the representation]
   *  @param Object pointer to object
   *  @param Address address
   *  @return status code
   */
  StatusCode createRep
  ( DataObject*      Object  ,
    IOpaqueAddress*& Address ) override;

  /** Update representation
   *  @param Object pointer to object
   *  @param Address address
   *  @return status code
   */
  StatusCode updateRep
  ( IOpaqueAddress*  Address, DataObject* Object ) override;

  /** class ID for converted objects
   */
  static const CLID&         classID();

  /** storage Type
   */
  static unsigned char storageType() ;

private:
  
  GiGaLeaf m_leaf;

};

// ============================================================================
// The End
// ============================================================================
#endif // COMPONENT_GIGALASSEMBLYCNV_H
// ============================================================================
