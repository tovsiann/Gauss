/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaCatalogCnv.h,v 1.4 2009-10-15 10:00:14 silviam Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.3  2003/12/10 14:04:24  ranjard
// v14r0 - fix for Gaudi v13r0
//
// Revision 1.2  2002/01/22 18:24:43  ibelyaev
//  Vanya: update for newer versions of Geant4 and Gaudi
//
// Revision 1.1  2001/11/19 18:27:00  ibelyaev
//  bux fix and the new converter for catalogs
//
// ============================================================================
#ifndef GIGACNV_GIGACATALOGCNV_H
#define GIGACNV_GIGACATALOGCNV_H 1
/// include files
/// GiGaCnv
#include "GiGaCnv/GiGaCnvBase.h"
#include "GiGaCnv/GiGaLeaf.h"

/** @class GiGaCatalogCnv GiGaCatalogCnv.h
 *
 *  Auxilalry converter for Catalog ("DataObject").
 *
 *  @author  Vanya Belyaev
 *  @date    19/11/2001
 */

class GiGaCatalogCnv :
  public GiGaCnvBase
{
public:
  
  /** Standard Constructor
   *  @param loc pointer to service locator
   */
  GiGaCatalogCnv( ISvcLocator* loc );

  /** destructor, virtual and protected
   */
  virtual ~GiGaCatalogCnv();

public:

  /** initialize the converter
   *  @return status code
   */
  StatusCode initialize() override;

  /** finalize  the converter
   *  @return status code
   */
  StatusCode finalize  () override;

  /** create the representation of the Object
   *  @param  Object  object
   *  @param  Address address
   *  @return status  code
   */
  StatusCode createRep
  ( DataObject*      Object  ,
    IOpaqueAddress*& Address ) override;

  /** update the representation of Object
   *  @param Object   object
   *  @param Address  address
   *  @return status  code
   */
  StatusCode updateRep
  ( IOpaqueAddress*  Address ,
    DataObject*      Object  ) override;

  /** retrieve the class identifier  for created object
   *  @return class idenfifier for created object
   */
  static const CLID&          classID();

  /** retrieve the storage type for created object
   *  @return storage type  for created object
   */
  //static const unsigned char storageType() ;
  static long storageType() ;

private:

  GiGaLeaf m_leaf ;

};


// ============================================================================
// The End
// ============================================================================
#endif // GIGACNV_GIGACATALOGCNV_H
// ============================================================================
