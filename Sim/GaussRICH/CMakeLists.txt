###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: GaussRICH
################################################################################
gaudi_subdir(GaussRICH v15r0)

gaudi_depends_on_subdirs(Det/RichDet
                         Event/LinkerEvent
                         Event/MCEvent
                         Kernel/Relations
                         Rich/RichKernel
                         Sim/GaussTools)

find_package(AIDA)
find_package(ROOT COMPONENTS Tree)

include_directories(src/SensDet src/PhysProcess src/RichAction src/Misc
                    src/PhysPhotDet src/RichAnalysis /src/srcG4)

find_package(Boost)
find_package(CLHEP)
find_package(HepMC)
find_package(Vc)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS} ${Vc_INCLUDE_DIR})

gaudi_add_library(GaussRICHLib
                  src/Misc/*.cpp
                  src/PhysPhotDet/*.cpp
                  src/srcG4/*.cc
                  src/SensDet/*.cpp
                  PUBLIC_HEADERS GaussRICH
                  INCLUDE_DIRS AIDA
                  LINK_LIBRARIES RichDetLib LinkerEvent MCEvent RelationsLib RichKernelLib GaussToolsLib ROOT)

gaudi_add_module(GaussRICH
                 src/*.cpp
                 src/PhysProcess/*.cpp
                 src/RichAnalysis/*.cpp
                 src/RichAction/*.cpp
                 src/Assoc/*.cpp
                 INCLUDE_DIRS AIDA
                 LINK_LIBRARIES RichDetLib LinkerEvent MCEvent RelationsLib
                                RichKernelLib GaussToolsLib GaussRICHLib
                 GENCONF_PRELOAD GaussToolsGenConfHelperLib)

gaudi_env(SET GAUSSRICHOPTS \${GAUSSRICHROOT}/options)
gaudi_install_python_modules()

