/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaussRICH/RichHpdPSF.h"


RichHpdPSF::RichHpdPSF()
  : m_hpdPointSpreadFunctionVect(4),
    m_hpdPSFPhoEnergyVect(4),
    m_hpdPSFRadialPosVect(4),
    m_hitSmearValueVect(3)
 {  }
// the 4 above is just a dummy value.

RichHpdPSF::~RichHpdPSF() { }

void RichHpdPSF::resetPSFVect() {

  m_hpdPointSpreadFunctionVect.clear();
  m_hpdPSFPhoEnergyVect.clear();
  m_hpdPSFRadialPosVect.clear();
  m_hitSmearValueVect.clear();
  
}


