/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GAUSSGEO_GAUSSGEOASSEMBLYSTORE_H_
#define GAUSSGEO_GAUSSGEOASSEMBLYSTORE_H_

// Standard
#include <vector>

// Gaudi
#include "GaudiKernel/StatusCode.h"

class GaussGeoAssembly;

class GaussGeoAssemblyStore {
 public:
  friend class GaussGeoAssembly;
  typedef std::vector<GaussGeoAssembly*> Assemblies;

 protected:
  GaussGeoAssemblyStore();
  StatusCode addAssembly(GaussGeoAssembly* assembly);
  StatusCode removeAssembly(GaussGeoAssembly* assembly);

 public:
  ~GaussGeoAssemblyStore();

  static GaussGeoAssemblyStore* store();

  GaussGeoAssembly* assembly(const std::string& name);
  StatusCode clear();

 private:
  GaussGeoAssemblyStore(const GaussGeoAssemblyStore&);
  GaussGeoAssemblyStore& operator=(const GaussGeoAssemblyStore&);

  Assemblies m_assemblies;
};

#endif // GAUSSGEO_GAUSSGEOASSEMBLYSTORE_H_
