/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHERENKOVANALYSIS_CKVG4STEPANALYSIS11_H
#define CHERENKOVANALYSIS_CKVG4STEPANALYSIS11_H 1

// Include files
#include "GiGa/GiGaStepActionBase.h"

template <class TYPE> class GiGaFactory;
class G4Step;



/** @class CkvG4StepAnalysis11 CkvG4StepAnalysis11.h CherenkovAnalysis/CkvG4StepAnalysis11.h
 *
 *
 *  @author Sajan Easo
 *  @date   2013-02-15
 */
class CkvG4StepAnalysis11: virtual public  GiGaStepActionBase  {
  friend class GiGaFactory<CkvG4StepAnalysis11>;


public:
  /// Standard constructor
  CkvG4StepAnalysis11(const std::string& type   ,
                        const std::string& name   ,
                        const IInterface*  parent  );

  ~CkvG4StepAnalysis11( ); ///< Destructor

  void UserSteppingAction( const G4Step* aStep ) override;

protected:

private:

  CkvG4StepAnalysis11();
  CkvG4StepAnalysis11(const  CkvG4StepAnalysis11&);
  CkvG4StepAnalysis11&  operator=(const CkvG4StepAnalysis11& );

};
#endif // CHERENKOVANALYSIS_CKVG4STEPANALYSIS11_H


