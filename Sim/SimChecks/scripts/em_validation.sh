#!/usr/bin/env sh
###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

OUTDIR=$PWD
RUNDIR=`mktemp -d`
cd $RUNDIR
echo "Continuing in: `pwd`"

# Generate rootFiles
cp $SIMCHECKSROOT/options/EmValidation/GenerateRootFiles.py .
cp $SIMCHECKSROOT/options/EmValidation/configurations.py .
cp $SIMCHECKSROOT/options/EmValidation/runTest.py .
python GenerateRootFiles.py

# Convert rootFiles to LHCbPR rootFiles
cp $SIMCHECKSROOT/python/EmValidation/analysisLHCbPR_Histograms.py .
python analysisLHCbPR_Histograms.py

mv *root ${OUTPUTDIR:-$OUTDIR}
cd ${OUTDIR}
