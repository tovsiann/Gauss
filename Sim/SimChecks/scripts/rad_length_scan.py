#!/usr/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#################################################################################
## This file contains a class for initiating sub-detector scan of LHCb         ##
## using the radiation length tool to obtain information on interaction and    ##
## radiation lengths.                                                          ##
##                                                                             ##
## This test can be run using the command:                                     ##
##                                                                             ##
##   python $SIMCHECKSROOT/rad_length_scan.py                                  ##
##                                                                             ##
## from within Gauss with the optional arguments:                              ##
##                                                                             ##
##  --giga-geo            Use GiGaGeo for Geometry reading                     ##
##  --gauss-geo           Use GaussGeo for Geometry reading                    ##
##  --no-plots            Run Test Only, No Plotting (for debugging)           ##
##  --plot-only           Repeat ploting (for debugging)                       ##
##  --zlim                Set Z limit on Velo and Rich1 profiles               ##
##                        to remove saturation due to beam pipe                ##
##  --year                Year tags to use within Gauss (Default: Latest)      ##
##  --debug               Run with higher verbosity                            ##
##  --debug-gauss         Run Gauss in Debug mode                              ##
##                                                                             ##
## Twiki at: https://twiki.cern.ch/twiki/bin/view/LHCb/RadLengthStudies        ##
##                                                                             ##
##  @author : L. Pescatore, K. Zarebski, Benedetto G. Siddi                    ##
##  @date   : last modified on 2020-11-26                                      ##
#################################################################################

from __future__ import print_function
import sys
import os
import logging
import subprocess
import glob

from RadLengthMakePlots import makePlots
from tempfile import NamedTemporaryFile

class rad_length_detector_scan:
    '''Class for Performing a Scan of SubDetectors within LHCb to extract Information on Radiation
and Interaction Lengths within the Different Regions using the Implemented Scoring Planes.

        Args:

            output_dir (string)                       :    the location where output files should be written to
          
            pdfs_dir (string)                         :    if specified, produce Pdfs of plots and save to this location
  
            text_data_dir (string)                    :    if specified, produce LaTeX tables and JSON output of results

            debug ('INFO'/'DEBUG')                    :    debug mode, default is 'INFO'

            use_geo ('Default'/'GaussGeo'/'GiGaGeo')  :    use either GaussGeo or GiGaGeo to read geometry, default 'Default'
                                                           is to use whichever is the default option in the version of Gauss

            prof_z_lim   (True/False)                 :    apply z limits to 'Rich1' and 'Velo' profiles to avoid saturation
                                                           due to high inter/rad length within the beam pipe

            gauss_year_tags (int)                     :    year to use for Gauss-XXXX.py

            debug_gauss  (True/False)                 :    launch Gauss in Debug mode for more output from RadLengthColl etc.
    '''
    _simchecks_version = os.environ['SIMCHECKSROOT']
    _radlength_tag_opt = '''
from Configurables import LHCbApp
from Configurables import CondDB
CondDB().Upgrade     = {use_upgrade}
LHCbApp().DDDBtag = "{dddb_tag}"
'''
    _radlength_opts    = os.path.join(_simchecks_version, 'options', 'RadLength')
    _lhcb_nophys_opts  = os.path.join(os.environ['APPCONFIGOPTS'], 'Gauss', 'G4PL_FTFP_BERT_EmNoCuts_noLHCbphys.py')
    _ana_opts          = os.path.join(_radlength_opts, 'RadLengthAna_NoVELO.py')
    _ana_velo_opts     = os.path.join(_radlength_opts, 'RadLengthAna_VELO.py')
    _ana_cumul_opts    = os.path.join(_radlength_opts, 'RadLengthAna_FullGeo.py')
    _mat_eval_opts     = os.path.join(_radlength_opts, 'MaterialEvalGun.py')
    _job_opts          = os.path.join(_radlength_opts, 'Gauss-Job.py')
    _debug_opts        = ""
    _gauss_cmd         = 'gaudirun.py {physoff}'.format(physoff=_lhcb_nophys_opts)
    upgrade = False
    def __init__(self, output_dir, pdfs_dir=None, text_data_dir=None, debug='INFO', use_geo='Default', 
                 gauss_year_tags=None, debug_gauss=False, prof_z_lim=False, upgrade=False, dddb=None):
        self._logger = logging.getLogger('RadLengthDetectorScan')
        logging.basicConfig()
        self.upgrade = upgrade
        self._logger.setLevel(debug)
   
        self._hist_lim = prof_z_lim
        if upgrade:
            self._year = "Upgrade"
        else:
            self._year = gauss_year_tags
        self._gauss_year_opts = self._select_gauss_year_tags(self._year)

        self._radlength_tag_opt = self._radlength_tag_opt.format(
            use_upgrade=upgrade,
            dddb_tag=dddb)

        if upgrade:
            self._ana_opts = self._ana_opts.replace(".py","_Upgrade.py")
            self._ana_cumul_opts = self._ana_cumul_opts.replace(".py","_Upgrade.py")
            self._ana_velo_opts = self._ana_velo_opts.replace(".py","_Upgrade.py")
        if debug_gauss:
           self._debug_opts = '''
from Configurables import ApplicationMgr
MessageSvc().OutputLevel = DEBUG
'''        
        if use_geo == 'GaussGeo':
           from Configurables import Gauss
           if 'UseGaussGeo' in dir(Gauss()):
               Gauss().UseGaussGeo = True
           else:
               self._logger.warning( "Option 'UseGausGeo' not available, using default Geometry reader" )
        elif use_geo == 'GiGaGeo':
           from Configurables import Gauss
           if 'UseGaussGeo' in dir(Gauss()):
               Gauss().UseGaussGeo = False
           else:
               self._logger.warning( "Option 'UseGausGeo' not available, using default Geometry reader" )

        sys.path.append(os.path.join(self._simchecks_version, 'python'))
        self._root_file_dir = os.path.join(output_dir, 'RadLengthDetectorScan', 'root_files')
        self._root_out_files = [os.path.join(self._root_file_dir, 'Rad_P2P.root')]
        self._root_out_files.append(os.path.join(self._root_file_dir, 'Rad_Cumul.root'))
        self.pdf_directory  = pdfs_dir
        self.text_data_directory = text_data_dir

        # Set Up the Directory Structure
        self._logger.info('Creating Directories for Test Output')
        folders = ['root_files']
        if self.pdf_directory:
           self._debug('Creating Folder for Pdfs')
           folders.append('pdf_files')
        if self.text_data_directory:
           self._debug('Creating Folder for JSON Data Tables')
           folders.append('data_tables')

        for folder in folders:
            if not os.path.exists(os.path.join(output_dir, 'RadLengthDetectorScan', folder)): #Let's Be Safe!
                os.makedirs(os.path.join(output_dir, 'RadLengthDetectorScan', folder))

    def _select_gauss_year_tags(self, tags):
        if tags:
            _opt_file = os.path.join(os.environ['GAUSSOPTS'], 'Gauss-{}.py'.format(tags))
            try:
                assert os.path.isfile(_opt_file)
            except AssertionError as e:
                self._logger.error("Option file '{}' Not Found!".format(_opt_file))
                raise e
        else:
            _opt_file = sorted(glob.glob(os.path.join(os.environ['GAUSSOPTS'], 'Gauss-20*.py')))[-1]
        self._logger.info("Using Option '{}' for Year tags".format(_opt_file))
        return _opt_file

    def _mergeOutput(self):
        '''Merge All ROOT files Produced During the Scan Into a Single File "Rad_merged.root"'''
        self._logger.debug("Merging ROOT Files")
        _files_to_merge = [os.path.join(self._root_file_dir, file_) for file_ in ['Rad_NoVELO.root', 'Rad_VELO.root']]
        _files_to_merge2 = [os.path.join(self._root_file_dir, file_) for file_ in ['Rad_FullGeo.root', 'Rad_VELO.root']]
        merge_command = 'hadd -f {output} {rootfiles}'.format(output=self._root_out_files[0], rootfiles=' '.join(_files_to_merge))
        subprocess.check_call(merge_command, shell=True)
        merge_command = 'hadd -f {output} {rootfiles}'.format(output=self._root_out_files[1], rootfiles=' '.join(_files_to_merge2))
        subprocess.check_call(merge_command, shell=True)

    def runScan(self, output_plots=True):
        '''Perform the Per Sub Detector Material Scan and Construct Radiation and Interaction Length Maps


               Args:
                      output_plots (True/False)    :   produce plots whilst running test, default is 'True'
        '''


        # Beyond 2012 Spillover was added to data taking options, this breaks Gauss with 'ParticleGun has no attribute "PileUpTool"' unless
        # a measure is taken to suppress it

        turn_off_spillover_opts = ''

        try:
            if not self._year or self._year > 2012:
                turn_off_spillover_opts = '$SIMCHECKSROOT/options/RadLength/SpilloverOff.py'
        except TypeError:
            if self._year == "Upgrade":
                turn_off_spillover_opts = '$SIMCHECKSROOT/options/RadLength/SpilloverOff.py'

        # Run Gauss using options for All Detectors but the VELO, then for the VELO itself

        for options, filename in zip([self._ana_opts, self._ana_velo_opts, self._ana_cumul_opts], ['Rad_NoVELO.root', 'Rad_VELO.root', 'Rad_FullGeo.root']):
            output_opts='''
from Gaudi.Configuration import *
# --- Save ntuple with hadronic cross section information
ApplicationMgr().ExtSvc += ["NTupleSvc"]
NTupleSvc().Output = ["FILE2 DATAFILE='{outfile}' TYP='ROOT' OPT='NEW'" ]
{radlength_tags}
{debug_opts}
'''.format(outfile=os.path.join(self._root_file_dir, filename),
                       radlength_tags=self._radlength_tag_opt,
                       debug_opts=self._debug_opts)

            with NamedTemporaryFile(suffix='.py') as tmp:
                tmp.write(str.encode(output_opts))
                tmp.flush()
                self._logger.debug('Running Gauss with Options: %s %s %s %s',self._gauss_cmd, options, self._job_opts, tmp.name)
                subprocess.check_call('{core} {year} {job_opts} {opts} {outfile_opts} {spill_opts} {material} '.format(
                    core=self._gauss_cmd, 
                    job_opts=self._job_opts,
                    year=self._gauss_year_opts,
                    opts=options, 
                    spill_opts=turn_off_spillover_opts,
                    material=self._mat_eval_opts,
                    outfile_opts=tmp.name), shell=True)

        # Merge the ROOT Files
        self._mergeOutput()

        # Run Plot Creation
        if output_plots:
            self._runPlotting()
 
        else:
            self._logger.warning("Option to Produce Plots upon Test Completion set to 'False'")

    def _runPlotting(self):
        '''Create Plots from the Results of the SubDetector Scans Including 2d Maps'''
        # Make the Plots using RadLengthMakePlots
        self._logger.info('Test Complete. Creating Plots using RadLengthMakePlots')
        makePlots(self._root_out_files, plot_type="rad",
                  output_dir=self._root_file_dir,
                  pdfs_dir=self.pdf_directory,
                  upgrade=self.upgrade,
                  data_dir=self.text_data_directory,
                  debug=self._logger.getEffectiveLevel(), max_z=self._hist_lim)

        makePlots(self._root_out_files, plot_type="inter",
                  output_dir=self._root_file_dir,
                  pdfs_dir=self.pdf_directory,
                  upgrade=self.upgrade,
                  data_dir=self.text_data_directory,
                  debug=self._logger.getEffectiveLevel(), max_z=self._hist_lim)

        inter_and_rad_plots = [os.path.join(self._root_file_dir, file_) for file_ in ['Interaction_Length_Plots.root', 'Radiation_Length_Plots.root']]

        self._logger.debug('Merging Plot Component Files')
        subprocess.check_call('hadd -f {output} {components}'.format(output=os.path.join(self._root_file_dir, 'RadLengthSubDetectorPlots.root'),
                              components=' '.join(inter_and_rad_plots)), shell=True)

        self._logger.debug('Deleting Plot Component Files')
        subprocess.check_call('rm {}'.format(' '.join(inter_and_rad_plots)), shell=True)

if __name__ == "__main__":

    #-------------------------------ARGUMENT PARSER---------------------------------#
    #               Run Test in 'Debug' mode if '--debug' flag set                  #
    #               Options to use GaussGeo or GiGaGeo                              #
    #               Choose whether to run plotting (and/or) test                    #
    #-------------------------------------------------------------------------------#

    help_str='''
Usage: rad_length_scan.py [--Help] [--debug] [--debug-gauss] [--gauss-geo/--giga-geo] [--plot-only/--no-plots] [--pdfs] [--zlim]

optional arguments:

--debug        run test in debug mode for more output

--debug-gauss  run Gauss in debug mode

--gauss-geo    run test using GaussGeo for geometry reading (if available)

--giga-geo     run test using GiGaGeo for geometry reading (if available)

--plot-only    run test with plot making only (assumes ROOT files exit)

--no-plots     run test without making plots

--zlim         apply a cap to Rich1 and Velo plots to avoid saturation from beam pipe at 4.3 < eta < 4.7

--Help         print this help message
'''
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true', help='Run in Debug Mode')
    parser.add_argument('--debug-gauss', action='store_true', help='Run Gauss in Debug Mode')
    parser.add_argument('--plot-only', action='store_true', help='Run Plotting Section of Test Only')
    parser.add_argument('--no-plots', action='store_true', help='Run Test without Making Plots')
    parser.add_argument('--gauss-geo', action='store_true', help='Run Test using GaussGeo')
    parser.add_argument('--giga-geo', action='store_true', help='Run Test using GiGaGeo')
    parser.add_argument('--year', help='Year tags for Gauss, Gauss-XXXX.py option used', default=2018)
    parser.add_argument('--upgrade', default=False, action='store_true', help='Use of Upgrade tags')
    parser.add_argument('--tags', default="radlength-scoring-bsiddi", help='DDDB tags to be used')       
    parser.add_argument('--zlim', action='store_true', help='Run with Z max applied to Rich1 and Velo profiles')
    parser.add_argument('--Help', action='store_true', help='Print help')
    args = parser.parse_args()

    if args.Help:
      print(help_str)
      exit(0)

    #-------------------------------CHOOSE GEO READER-------------------------------#

    geo_type = 'Default'
    debug_mode = "INFO"
    if args.giga_geo:
       geo_type = 'GiGaGeo'

    if args.gauss_geo:
       geo_type = 'GaussGeo'

    mode = 'FullTest' if not args.no_plots else 'NoPlots'
    mode = 'FullTest' if not args.plot_only else 'PlotOnly'

    if args.debug:
        debug_mode = "DEBUG"
    #----------------------------------COMMENCE TEST--------------------------------#


    opts_str='''
============= RADLENGTH SUBDETECTOR SCAN ==============
 
 Running scan across each subdetector of LHCb with
 the following options:

 Geometry Reader          :\t{g}
 Debug Info (Gauss)       :\t{d}
 Debug Info (Test)        :\t{d2}
 Rich1/Velo Z Limit       :\t{z}
 Mode                     :\t{m}
 Year                     :\t{y}
 Upgrade                  :\t{u}
 Tags                     :\t{t}
=======================================================
'''.format(g=geo_type, d=args.debug_gauss, d2=args.debug, z=args.zlim, m=mode,
           y=args.year if args.year else 'Latest', u=args.upgrade, t=args.tags)

    print(opts_str)
    

    #-------------------------------------------------------------------------------#

    rad_length_scan = rad_length_detector_scan(os.getcwd(), debug=debug_mode, use_geo=geo_type, 
                                               debug_gauss=args.debug_gauss, prof_z_lim=args.zlim,
                                               gauss_year_tags=args.year, upgrade=args.upgrade, dddb=args.tags)

    #-----------------------------CHECK PLOTTING OPTIONS----------------------------#

    if args.plot_only:
       for type_ in ['Interaction', 'Radiation']:
           if os.path.exists(os.path.join(os.getcwd(), 'RadLengthDetectorScan', 'root_files', '{}_Length_Plots.root'.format(type_))):
              subprocess.check_call('rm {}'.format(os.path.join(os.getcwd(), 'RadLengthDetectorScan', 'root_files', '{}_Length_Plots.root'.format(type_))), shell=True)
       rad_length_scan._runPlotting()
    elif args.no_plots:
       rad_length_scan.runScan(False)
    else:
       rad_length_scan.runScan()
