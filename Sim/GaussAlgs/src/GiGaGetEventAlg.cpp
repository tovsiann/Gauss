/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 

// from Gaudi
#include "GaudiKernel/Stat.h"

// Event 
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"

// local 
#include "GiGaGetEventAlg.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GiGaGetEvent
// 
// 2004-02-20 : Vanya Belyaev 
// 2005-02-02 : Gloria Corti   
// 2006-01-16 : Gloria Corti
// 2007-01-11 : Gloria Corti
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GiGaGetEventAlg )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
GiGaGetEventAlg::GiGaGetEventAlg(const std::string& name, 
                                 ISvcLocator* pSvcLocator) 
  : GaudiAlgorithm( name , pSvcLocator) 
{ 
  declareProperty( "Particles", m_particles = LHCb::MCParticleLocation::Default); 
  declareProperty( "Vertices" , m_vertices = LHCb::MCVertexLocation::Default); 
}

//=============================================================================
// Destructor
//=============================================================================
GiGaGetEventAlg::~GiGaGetEventAlg(){}

//=============================================================================
// Main execution
//=============================================================================
StatusCode GiGaGetEventAlg::execute() 
{
  if ( !m_particles.empty() )
  {
    const LHCb::MCParticles* particles = get<LHCb::MCParticles>( m_particles );
    info() << "Number of extracted MCParticles '"
           << m_particles << "' \t"
           << particles -> size() 
           << endmsg ;
    Stat stat( chronoSvc(), "#MCParticles", particles->size() );
  }
  
  if( !m_vertices.empty() )
  {
    const LHCb::MCVertices* vertices = get<LHCb::MCVertices>( m_vertices ) ;
    info() << "Number of extracted MCVertices  '"
           << m_vertices << "'  \t" 
           << vertices -> size() 
           << endmsg ;
    Stat stat( chronoSvc(), "#MCVertices", vertices->size() );
  }
  
  return StatusCode::SUCCESS;
}

//=============================================================================
