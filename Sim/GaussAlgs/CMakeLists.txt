###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: GaussAlgs
################################################################################
gaudi_subdir(GaussAlgs v8r1)

gaudi_depends_on_subdirs(Kernel/LHCbKernel
                         Sim/GaussTools)

find_package(Boost)
find_package(CLHEP COMPONENTS Random Vector)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(GaussAlgs
                 src/*.cpp
                 INCLUDE_DIRS Boost
                 LINK_LIBRARIES CLHEP Boost LHCbKernel GaussToolsLib)

