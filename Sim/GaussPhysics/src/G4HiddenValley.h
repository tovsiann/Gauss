/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef G4HIDDENVALLEY_H 
#define G4HIDDENVALLEY_H 1

#include "Geant4/G4ParticleDefinition.hh"

/* ###############################################################################
 * ### Hidden Valley particles for Dark Showers (HV processes in Pythia 8.230) ###
 * ### G4Higgses used as a template to write this file (author: Vanya Belyaev) ###
 * ### 2018-02-03 - written by Carlos Vazquez Sierra (carlos.vazquez@cern.ch)  ###
 * ### 2020-03-09 - revisited by CVS for inclusion on Gauss master             ###
 * ###############################################################################
 * id="4900111" name="pivDiag" spinType="1" chargeType="0" colType="0" m0="10.00000" 
 * id="4900113" name="rhovDiag" spinType="3" chargeType="0" colType="0" m0="10.00000" 
 * id="4900023" name="Zv" spinType="3" chargeType="0" colType="0" m0="1000.00000" mWidth="20.00000" mMin="100.00000" mMax="0.00000" 
 * id="4900101" name="qv" antiName="qvbar" spinType="1" chargeType="0" colType="0" m0="100.00000"
 * id="4900211" name="pivUp" antiName="pivDn" spinType="1" chargeType="0" colType="0" m0="10.00000"
 * id="4900213" name="rhovUp" antiName="rhovDn" spinType="3" chargeType="0" colType="0" m0="10.00000"
 * id="4900021" name="gv" spinType="3" chargeType="0" colType="0" m0="0.00000" 
 */

// ================================================================================================
// id="4900111" name="pivDiag" spinType="1" chargeType="0" colType="0" m0="10.00000" 
// id="4900211" name="pivUp" antiName="pivDn" spinType="1" chargeType="0" colType="0" m0="10.00000"
// ================================================================================================
class G4pivDiag: public G4ParticleDefinition 
{
public:
  static G4pivDiag* Definition     () ;
  static G4pivDiag* pivDiagDefinition () ;
  static G4pivDiag* pivDiag           () ;
public:
  static G4pivDiag* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4pivDiag ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4pivDiag() ;  // virtual desctructor 
private:
  G4pivDiag () ;                               // default constructor is disabled 
  G4pivDiag ( const G4pivDiag& ) ;                    // copy constructor is disabled 
  G4pivDiag& operator=( const G4pivDiag& ) ;       // assignment operator is disabled 
private:
  static G4pivDiag* s_instance ;
} ;

// ================================================================================================
class G4pivUp: public G4ParticleDefinition 
{
public:
  static G4pivUp* Definition     () ;
  static G4pivUp* pivUpDefinition () ;
  static G4pivUp* pivUp           () ;
public:
  static G4pivUp* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4pivUp ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4pivUp() ;  // virtual desctructor 
private:
  G4pivUp () ;                               // default constructor is disabled 
  G4pivUp ( const G4pivUp& ) ;                    // copy constructor is disabled 
  G4pivUp& operator=( const G4pivUp& ) ;       // assignment operator is disabled 
private:
  static G4pivUp* s_instance ;
} ;

// ================================================================================================

class G4pivDn: public G4ParticleDefinition 
{
public:
  static G4pivDn* Definition     () ;
  static G4pivDn* pivDnDefinition () ;
  static G4pivDn* pivDn           () ;
public:
  static G4pivDn* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4pivDn ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4pivDn() ;  // virtual desctructor 
private:
  G4pivDn () ;                               // default constructor is disabled 
  G4pivDn ( const G4pivDn& ) ;                    // copy constructor is disabled 
  G4pivDn& operator=( const G4pivDn& ) ;       // assignment operator is disabled 
private:
  static G4pivDn* s_instance ;
} ;

// ==================================================================================================
// id="4900113" name="rhovDiag" spinType="3" chargeType="0" colType="0" m0="10.00000" 
// id="4900213" name="rhovUp" antiName="rhovDn" spinType="3" chargeType="0" colType="0" m0="10.00000"
// ==================================================================================================
class G4rhovDiag: public G4ParticleDefinition 
{
public:
  static G4rhovDiag* Definition     () ;
  static G4rhovDiag* rhovDiagDefinition () ;
  static G4rhovDiag* rhovDiag           () ;
public:
  static G4rhovDiag* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4rhovDiag ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4rhovDiag() ;  // virtual desctructor 
private:
  G4rhovDiag () ;                               // default constructor is disabled 
  G4rhovDiag ( const G4rhovDiag& ) ;                    // copy constructor is disabled 
  G4rhovDiag& operator=( const G4rhovDiag& ) ;       // assignment operator is disabled 
private:
  static G4rhovDiag* s_instance ;
} ;

// ================================================================================================
class G4rhovUp: public G4ParticleDefinition 
{
public:
  static G4rhovUp* Definition     () ;
  static G4rhovUp* rhovUpDefinition () ;
  static G4rhovUp* rhovUp           () ;
public:
  static G4rhovUp* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4rhovUp ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4rhovUp() ;  // virtual desctructor 
private:
  G4rhovUp () ;                               // default constructor is disabled 
  G4rhovUp ( const G4rhovUp& ) ;                    // copy constructor is disabled 
  G4rhovUp& operator=( const G4rhovUp& ) ;       // assignment operator is disabled 
private:
  static G4rhovUp* s_instance ;
} ;

// ================================================================================================

class G4rhovDn: public G4ParticleDefinition 
{
public:
  static G4rhovDn* Definition     () ;
  static G4rhovDn* rhovDnDefinition () ;
  static G4rhovDn* rhovDn           () ;
public:
  static G4rhovDn* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4rhovDn ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4rhovDn() ;  // virtual desctructor 
private:
  G4rhovDn () ;                               // default constructor is disabled 
  G4rhovDn ( const G4rhovDn& ) ;                    // copy constructor is disabled 
  G4rhovDn& operator=( const G4rhovDn& ) ;       // assignment operator is disabled 
private:
  static G4rhovDn* s_instance ;
} ;

// ================================================================================================================================
// id="4900023" name="Zv" spinType="3" chargeType="0" colType="0" m0="1000.00000" mWidth="20.00000" mMin="100.00000" mMax="0.00000" 
// ================================================================================================================================
class G4Zv: public G4ParticleDefinition 
{
public:
  static G4Zv* Definition     () ;
  static G4Zv* ZvDefinition () ;
  static G4Zv* Zv           () ;
public:
  static G4Zv* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4Zv ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4Zv() ;  // virtual desctructor 
private:
  G4Zv () ;                               // default constructor is disabled 
  G4Zv ( const G4Zv& ) ;                    // copy constructor is disabled 
  G4Zv& operator=( const G4Zv& ) ;       // assignment operator is disabled 
private:
  static G4Zv* s_instance ;
} ;

// ==============================================================================
// * id="4900021" name="gv" spinType="3" chargeType="0" colType="0" m0="0.00000" 
// ==============================================================================
class G4gv: public G4ParticleDefinition 
{
public:
  static G4gv* Definition     () ;
  static G4gv* gvDefinition () ;
  static G4gv* gv           () ;
public:
  static G4gv* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4gv ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4gv() ;  // virtual desctructor 
private:
  G4gv () ;                               // default constructor is disabled 
  G4gv ( const G4gv& ) ;                    // copy constructor is disabled 
  G4gv& operator=( const G4gv& ) ;       // assignment operator is disabled 
private:
  static G4gv* s_instance ;
} ;

// ==============================================================================================
// id="4900101" name="qv" antiName="qvbar" spinType="1" chargeType="0" colType="0" m0="100.00000"
// ==============================================================================================
class G4qv: public G4ParticleDefinition 
{
public:
  static G4qv* Definition     () ;
  static G4qv* qvDefinition () ;
  static G4qv* qv           () ;
public:
  static G4qv* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4qv ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4qv() ;  // virtual desctructor 
private:
  G4qv () ;                               // default constructor is disabled 
  G4qv ( const G4qv& ) ;                    // copy constructor is disabled 
  G4qv& operator=( const G4qv& ) ;       // assignment operator is disabled 
private:
  static G4qv* s_instance ;
} ;

// ==============================================================================================

class G4qvbar: public G4ParticleDefinition 
{
public:
  static G4qvbar* Definition     () ;
  static G4qvbar* qvbarDefinition () ;
  static G4qvbar* qvbar           () ;
public:
  static G4qvbar* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4qvbar ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4qvbar() ;  // virtual desctructor 
private:
  G4qvbar () ;                               // default constructor is disabled 
  G4qvbar ( const G4qvbar& ) ;                    // copy constructor is disabled 
  G4qvbar& operator=( const G4qvbar& ) ;       // assignment operator is disabled 
private:
  static G4qvbar* s_instance ;
} ;

// ============================================================================
// The END 
// ============================================================================
#endif // G4HIDDENVALLEY_H
// ============================================================================

