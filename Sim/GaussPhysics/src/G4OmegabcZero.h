/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef G4OmegabcZero_h
#define G4OmegabcZero_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         OmegabcZero                        ###
// ######################################################################

class G4OmegabcZero : public G4ParticleDefinition
{
 private:
  static G4OmegabcZero * theInstance ;
  G4OmegabcZero( ) { }
  ~G4OmegabcZero( ) { }


 public:
  static G4OmegabcZero * Definition() ;
  static G4OmegabcZero * OmegabcZeroDefinition() ;
  static G4OmegabcZero * OmegabcZero() ;
};


#endif
