/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef G4AntiOmegaccMinus_h
#define G4AntiOmegaccMinus_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         AntiOmegaccMinus                        ###
// ######################################################################

class G4AntiOmegaccMinus : public G4ParticleDefinition
{
 private:
  static G4AntiOmegaccMinus * theInstance ;
  G4AntiOmegaccMinus( ) { }
  ~G4AntiOmegaccMinus( ) { }


 public:
  static G4AntiOmegaccMinus * Definition() ;
  static G4AntiOmegaccMinus * AntiOmegaccMinusDefinition() ;
  static G4AntiOmegaccMinus * AntiOmegaccMinus() ;
};


#endif
