/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4AntiXibcZero_h
#define G4AntiXibcZero_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         AntiXibcZero                        ###
// ######################################################################

class G4AntiXibcZero : public G4ParticleDefinition
{
 private:
  static G4AntiXibcZero * theInstance ;
  G4AntiXibcZero( ) { }
  ~G4AntiXibcZero( ) { }


 public:
  static G4AntiXibcZero * Definition() ;
  static G4AntiXibcZero * AntiXibcZeroDefinition() ;
  static G4AntiXibcZero * AntiXibcZero() ;
};


#endif
