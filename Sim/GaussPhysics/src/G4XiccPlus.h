/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4XiccPlus_h
#define G4XiccPlus_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         XiccPlus                        ###
// ######################################################################

class G4XiccPlus : public G4ParticleDefinition
{
 private:
  static G4XiccPlus * theInstance ;
  G4XiccPlus( ) { }
  ~G4XiccPlus( ) { }


 public:
  static G4XiccPlus * Definition() ;
  static G4XiccPlus * XiccPlusDefinition() ;
  static G4XiccPlus * XiccPlus() ;
};


#endif
