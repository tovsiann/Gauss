/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4XibZero_h
#define G4XibZero_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         XibZero                        ###
// ######################################################################

class G4XibZero : public G4ParticleDefinition
{
 private:
  static G4XibZero * theInstance ;
  G4XibZero( ) { }
  ~G4XibZero( ) { }


 public:
  static G4XibZero * Definition() ;
  static G4XibZero * XibZeroDefinition() ;
  static G4XibZero * XibZero() ;
};


#endif
