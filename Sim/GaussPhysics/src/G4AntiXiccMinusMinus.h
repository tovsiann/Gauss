/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4AntiXiccMinusMinus_h
#define G4AntiXiccMinusMinus_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                      AntiXiccMinusMinus                        ###
// ######################################################################

class G4AntiXiccMinusMinus : public G4ParticleDefinition
{
 private:
  static G4AntiXiccMinusMinus * theInstance ;
  G4AntiXiccMinusMinus( ) { }
  ~G4AntiXiccMinusMinus( ) { }


 public:
  static G4AntiXiccMinusMinus * Definition() ;
  static G4AntiXiccMinusMinus * AntiXiccMinusMinusDefinition() ;
  static G4AntiXiccMinusMinus * AntiXiccMinusMinus() ;
};


#endif
