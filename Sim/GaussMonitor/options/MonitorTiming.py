###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# --- Configure the tool
from Configurables import GiGa, GiGaStepActionSequence, MonitorTiming

giga = GiGa()
giga.addTool(GiGaStepActionSequence("StepSeq"), name="StepSeq")
giga.StepSeq.addTool(MonitorTiming)
giga.StepSeq.MonitorTiming.OutputLevel = 0
giga.StepSeq.MonitorTiming.TimerFileName = 'Timing.log'

giga.StepSeq.MonitorTiming.Volumes = [
    'Velo', 'Rich1', 'TT', 'Magnet',
    'IT', 'OT', 'Rich2',
    'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon',
    'Pipe', 'Converter'
]

# Aggregate groups of volumes for summary
giga.StepSeq.MonitorTiming.DetsGroups = {
    'Velo': ['Tracking'],
    'TT':   ['Tracking'],
    'IT':   ['Tracking'],
    'OT':   ['Tracking'],
    'Rich1': ['RICH'],
    'Rich2': ['RICH'],
    'Ecal': ['Calo'],
    'Hcal': ['Calo'],
    'Spd': ['Calo'],
    'Prs': ['Calo']
}

# Need to run as post-config option: Add other memebers to StepSeq
giga.StepSeq.Members += ['MonitorTiming']

giga.StepSeq.Members += ['RichG4StepAnalysis6/RichStepHpdRefl']
giga.StepSeq.Members += ['RichG4StepAnalysis4/RichStepAgelExit']
giga.StepSeq.Members += ['RichG4StepAnalysis5/RichStepMirrorRefl']
giga.StepSeq.Members += ['GaussStepAction/GaussStep']
giga.StepSeq.Members += ['RichG4StepAnalysis4/RichStepAgelExit']
giga.StepSeq.Members += ['RichG4StepAnalysis5/RichStepMirrorRefl']
giga.StepSeq.Members += ['RichG4StepAnalysis3/RichStep']
