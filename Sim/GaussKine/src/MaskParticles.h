/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class MaskParticles MaskParticles.h
 *
 *  Algorithm to prevent certain classes of generated particles from
 *  being transmitted to the simulation phase by setting their status
 *  to DocumentationParticle.
 *
 */

class MaskParticles : public GaudiAlgorithm {

 public:
  /// Standard constructor
  MaskParticles(const std::string& name, ISvcLocator* pSvcLocator);
  /// Destructor
  virtual ~MaskParticles();

  StatusCode execute() override;       ///< Algorithm execution

 private:
  /// TES location of input HepMC events.
  std::string m_inputLocation;
  /// PDG ID of particles to be masked.
  int m_pid;
  /// Min. pseudorapidity required for a particle to be masked.
  double m_etaCut;
  /// Min. energy required for a particle to be masked.
  double m_energyCut;

};
