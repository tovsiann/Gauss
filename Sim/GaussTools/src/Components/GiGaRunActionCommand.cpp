/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaRunActionCommand.cpp,v 1.2 2007-01-12 15:36:54 ranjard Exp $
// Include files 

// from Gaudi
/// G4 
#include "Geant4/G4UImanager.hh"
/// Local 
#include "GiGaRunActionCommand.h"

// ============================================================================
/** @file 
 *
 *  Implementation file for class : GiGaRunActionCommand
 *
 *  @author Vanya  Belyaev Ivan.Belyaev@itep.ru
 *  @date 25/07/2001 
 */
// ============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( GiGaRunActionCommand )

// ============================================================================
/** standard constructor 
 *  @see GiGaPhysListBase
 *  @see GiGaBase 
 *  @see AlgTool 
 *  @param type type of the object (?)
 *  @param name name of the object
 *  @param parent  pointer to parent object
 */
// ============================================================================
GiGaRunActionCommand::GiGaRunActionCommand
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent ) 
  : GiGaRunActionBase( type , name , parent )
  , m_beginCmds ()   //  empty default list! 
  , m_endCmds   ()   //  empty default list! 
{  
  declareProperty("BeginOfRunCommands", m_beginCmds );
  declareProperty("EndOfRunCommands"  , m_endCmds   );
}


// ============================================================================
/// destructor 
// ============================================================================
GiGaRunActionCommand::~GiGaRunActionCommand()
{
  m_beginCmds .clear();
  m_endCmds   .clear();
}

// ============================================================================
/** performe the action at the begin of each run 
 *  @param run pointer to Geant4 run object 
 */
// ============================================================================
void GiGaRunActionCommand::BeginOfRunAction( const G4Run* run )
{
  if( 0 == run ) 
    { Warning("BeginOfRunAction:: G4Run* points to NULL!").ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
  /// get Geant4 UI manager 
  G4UImanager* ui = G4UImanager::GetUIpointer() ;
  if( 0 == ui    ) 
    { Error("BeginOfRunAction:: G4UImanager* points to NULL!").ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); return ; }
  else 
    {
      for( COMMANDS::const_iterator iCmd = m_beginCmds.begin() ;
           m_beginCmds.end() != iCmd ; ++iCmd ) 
        { 
          Print("BeginOfRunAction(): execute '" + (*iCmd) + "'" , 
                StatusCode::SUCCESS                             , MSG::DEBUG ).ignore();
          ui->ApplyCommand( *iCmd ); 
        }
    }
}


// ============================================================================
/** performe the action at the end of each run 
 *  @param run pointer to Geant4 run object 
 */
// ============================================================================
void GiGaRunActionCommand::EndOfRunAction( const G4Run* run )
{
  if( 0 == run ) 
    { Warning("EndOfRunAction:: G4Run* points to NULL!").ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
  /// get Geant4 UI manager 
  G4UImanager* ui = G4UImanager::GetUIpointer() ;
  if( 0 == ui    ) 
    { Error("EndOfRunAction:: G4UImanager* points to NULL!").ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
  else
    { 
      for( COMMANDS::const_iterator iCmd = m_endCmds.begin() ;
           m_endCmds.end() != iCmd ; ++iCmd ) 
        { 
          Print("EndOfRunAction(): execute '" + (*iCmd) + "'" , 
                StatusCode::SUCCESS                           , MSG::DEBUG ).ignore();
          ui->ApplyCommand( *iCmd ); 
        }
    }  
}


// ============================================================================
// The End
// ============================================================================

