###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: GaussTools
################################################################################
gaudi_subdir(GaussTools v20r1)

gaudi_depends_on_subdirs(Sim/GiGaCnv
                         Sim/SimSvc)

FindG4libs(run intercoms)
find_package(CLHEP COMPONENTS Random)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})


gaudi_add_library(GaussToolsLib
                  src/Lib/*.cpp
                  PUBLIC_HEADERS GaussTools
                  INCLUDE_DIRS Sim/SimSvc
                  LINK_LIBRARIES GiGaCnvLib)

gaudi_add_library(GaussToolsGenConfHelperLib
                  src/genConf/*.cpp
                  NO_PUBLIC_HEADERS
                  LINK_LIBRARIES CLHEP ${GEANT4_LIBS})

gaudi_add_module(GaussTools
                 src/Components/*.cpp
                 INCLUDE_DIRS Sim/SimSvc
                 LINK_LIBRARIES GiGaCnvLib GaussToolsLib ${GEANT4_LIBS}
                 GENCONF_PRELOAD GaussToolsGenConfHelperLib)

gaudi_add_unit_test(test_zMaxTilt tests/src/test_zMaxTilt.cpp
                    LINK_LIBRARIES GaudiKernel GaussToolsLib
                    TYPE Boost)
