/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
// ============================================================================
// Include files 
// ============================================================================
// STD & STL 
// ============================================================================
#include <iostream>
// ============================================================================

#ifdef WIN32
extern "C"  double __stdcall CPYR  ( int* )
#else
extern "C"  double           cpyr_ ( int* )
#endif
{
  std::cerr << "Invalid call for fake cpyr!" << std::endl ;
  return 1 ;
}
// ============================================================================
// The END 
// ============================================================================
