/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// local
#include "DiBosonType.h"

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenEvent.h"

#include "Kernel/ParticleProperty.h"
#include "Kernel/ParticleID.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DiBosonType
//
// 2020-Nov-18: Hang Yin
//
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DiBosonType )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DiBosonType::DiBosonType( const std::string & type , 
			  const std::string & name ,
			  const IInterface * parent )
: GaudiTool  ( type, name , parent ) ,
  m_thetaMax          ( 400 * Gaudi::Units::mrad ) ,
  m_thetaMin          ( 10 * Gaudi::Units::mrad  ) ,
  m_nLepton           ( 4                        ) ,
  m_nBoson            ( 2                        ) ,
  m_TypeLepton        (                          ) ,  
  m_ptMin             ( 10 * Gaudi::Units::GeV   ) ,
  m_leptonFromMother  ( true                     ) ,
  m_bosonFromMother   ( false                    ) ,
  m_MinMass           ( -1.                      ) ,
  m_motherofboson_id  ( "H_10"                   ) ,
  m_motherofboson_pid ( 0                        ) ,
  m_ppSvc             ( 0                        )
{
  declareInterface< IGenCutTool >( this ) ;

  // default properties are set for Higgs production with the 2b 
  // in the acceptance and a Lepton with pt>10GeV from WZ

  //Rough definition of the acceptance
  declareProperty( "ThetaMax" , m_thetaMax) ; //default=400 * mrad
  declareProperty( "ThetaMin" , m_thetaMin) ; //default=10 * mrad

  //Number of leptons requiered to be in the acceptance
  declareProperty( "NumberOfLepton" , m_nLepton) ; //max 4 leptons -- 
  //3 - 4 default=4
  declareProperty( "NumberOfBoson"  , m_nBoson) ; //max 2 bosons 

  m_TypeLepton.push_back ( "e+"  ) ;
  m_TypeLepton.push_back ( "mu+" ) ;                                                 
  //Type of leptons requiered to be in the acceptance // default e and mu
  declareProperty( "TypeOfLepton" , m_TypeLepton) ; 

  //LeptonPtMin is the minimum value the pt of lepton can have
  declareProperty( "LeptonPtMin" , m_ptMin) ; //default=10*GeV
	
  //LeptonIsFromMother enabled will requiered that the lepton comes from a specific mother
  declareProperty( "LeptonIsFromMother" , m_leptonFromMother) ; //default=true

  //MotherOfLepton is the list of particles from which the lepton comes
  declareProperty( "MotherOfLeptonMinMass" , m_MinMass) ;	//use for Z/gamma*

  //BosonIsFromMother enabled will requiered that the boson comes from a specific mother
  declareProperty( "BosonIsFromMother" , m_bosonFromMother) ; //default=false
  // MotherOfBoson define the mother of the W/Z boson
  declareProperty( "MotherOfBoson" , m_motherofboson_id) ; //default=Higgs H_10
}

//=============================================================================
// Destructor
//=============================================================================
DiBosonType::~DiBosonType( ) { ; }

//=============================================================================
// Initialise
//============================================================================
StatusCode DiBosonType::initialize() {
  StatusCode sc = GaudiTool::initialize() ;
  if ( ! sc.isSuccess() ) return sc ;
  
  if ( ( 2 != m_nLepton ) && ( 3 != m_nLepton )  && ( 4 != m_nLepton ) ) {
    fatal() << "The only choices for NumberOfLepton property are 2, 3, 4" 
            << endmsg;
    return StatusCode::FAILURE;
  }

  if ( ( 2 != m_nBoson ) && ( 1 != m_nBoson ) && ( 0 != m_nBoson ) ) {
    fatal() << "The only choices for NumberOfBoson property are 0, 1, 2" 
            << endmsg;
    return StatusCode::FAILURE;
  }

  m_ppSvc = svc<LHCb::IParticlePropertySvc>("LHCb::ParticlePropertySvc", true);
  m_motherofboson_pid = abs( m_ppSvc->find( m_motherofboson_id )->pdgID().pid() );

  return sc ;
}

//=============================================================================
// look for boson
//=============================================================================
bool DiBosonType::IsBoson( const HepMC::GenParticle * p ) const {
  bool isaboson = false ;
  
  for ( auto & thepid : m_motheroflepton ){
    
    if ( abs(p->pdg_id()) == abs (m_ppSvc->find(thepid)->pdgID().pid())){ 
      // boson's daughter must be leptons, not a media state boson
      for ( auto & idPart :  m_TypeLepton ){
	
	HepMC::GenVertex::particle_iterator iterchild ;
	HepMC::GenVertex * thePVchild =  p -> end_vertex() ;
	
	for(iterchild = thePVchild -> particles_begin( HepMC::children);
	    iterchild != thePVchild -> particles_end(HepMC::children); ++iterchild){
	  if( abs( m_ppSvc->find(idPart)->pdgID().pid() ) == abs( (*iterchild)->pdg_id() ) )
	    isaboson = true;
        } // for(iterchild)
      } // for(idPart)
    } // if (ID == W/Z)
  } // for (loop W/Z)

  // check mother of W/Z bosons or not
  if( isaboson == true ){
    if ( ! m_bosonFromMother ) return true ;
    else { // must decay from a Higgs boson
      HepMC::GenVertex * thePV =  p -> production_vertex() ;
      HepMC::GenVertex::particle_iterator iter ;
      for(iter = thePV -> particles_begin( HepMC::parents);
          iter != thePV -> particles_end(HepMC::parents); ++iter){
	std::string thepid = m_motherofboson_id;
	if( abs( m_ppSvc->find(thepid)->pdgID().pid() ) == abs( (*iter)->pdg_id() ) )
          if( (*iter)->momentum().m() > m_MinMass )
            return true;
      } // for(iter)
    } // check mother of W/Z  
  } // isaboson == true
  
  return false;
}

//=============================================================================
// look for lepton
//=============================================================================
bool DiBosonType::IsLepton( const HepMC::GenParticle * p ) const {
  bool isalepton = false ;
  
  for ( auto & thepid : m_TypeLepton ){
    if ( abs(p->pdg_id()) == abs (m_ppSvc->find(thepid)->pdgID().pid())) 
      isalepton = true;
  }
  
  if( isalepton == true ){
    if ( ! m_leptonFromMother ) return true ;
    else {
      HepMC::GenVertex * thePV =  p -> production_vertex() ;
      HepMC::GenVertex::particle_iterator iter ;
      for(iter = thePV -> particles_begin( HepMC::parents);
          iter != thePV -> particles_end(HepMC::parents); ++iter){
        for ( auto & thepid : m_motheroflepton ){
	  if( abs( m_ppSvc->find(thepid)->pdgID().pid() ) == abs( (*iter)->pdg_id() ) )
	    if( (*iter)->momentum().m() > m_MinMass )
	      return true;
	} // for (thepid)
      } // for (iter)
    } // else; check mother of leptons'
  } // isalepton == true
  return false;
}

//=============================================================================
// Accept function
//=============================================================================
bool DiBosonType::applyCut( ParticleVector & /* theParticleVector */ ,
			    const HepMC::GenEvent * theEvent ,
			    const LHCb::GenCollision * /* theCollision */ )
  const {
  // Selection of the lepton and boson
  std::vector< const HepMC::GenParticle * > LeptonList ;
  std::vector< const HepMC::GenParticle * > BosonList ;
  for ( HepMC::GenEvent::particle_const_iterator iterall = theEvent -> particles_begin() ;
        iterall!= theEvent -> particles_end() ; iterall++ ) {
    if ( IsLepton( *(iterall) ) ) LeptonList.push_back( *(iterall) ) ;
    if ( IsBoson(  *(iterall) ) ) BosonList.push_back( *(iterall) ) ;
  }

  if( ( 0 == LeptonList.size() ) ) {
    if( msgLevel( MSG::DEBUG ) ){
      debug()<<"No lepton in this event of requiered type "
             <<"with requiered mother (if mother was asked)!"<< endmsg;   
      debug()<<"You either produced events with no leptons, "
             <<"or put the wrong type of lepton, or of lepton mother"<< endmsg;
    }
    return false ;
  }

  if( ( 0 == BosonList.size() ) ) {
    if( msgLevel( MSG::DEBUG ) ){
      debug()<<"No boson in this event of requiered type "
             <<"with requiered mother (if mother was asked)!"<< endmsg;   
      debug()<<"You either produced events with no bosons, "
             <<"or put the wrong type of boson, or of boson mother"<< endmsg;
    }
    return false ;
  }

  // get pz, pt, theta for each leptons
  double pzl1,ptl1,thetal1;
  double pzl2,ptl2,thetal2;
  double pzl3,ptl3,thetal3;
  double pzl4,ptl4,thetal4;
  
  if ( m_nLepton <= ( int ) LeptonList.size() ) {
    const HepMC::GenParticle * theLepton1(0), *theLepton2(0), *theLepton3(0), *theLepton4(0);
    std::vector<const HepMC::GenParticle * >::iterator iterLepton = 
      LeptonList.begin() ;
    if ( 4 <= LeptonList.size() ) {
      theLepton1 = iterLepton[0];
      theLepton2 = iterLepton[1];
      theLepton3 = iterLepton[2];
      theLepton4 = iterLepton[3];
    }else if( 3== LeptonList.size() ) {
      theLepton1 = iterLepton[0];
      theLepton2 = iterLepton[1];
      theLepton3 = iterLepton[2];
      theLepton4 = iterLepton[2];
    }else if( 2== LeptonList.size() ) {
      theLepton1 = iterLepton[0];
      theLepton2 = iterLepton[1];
      theLepton3 = iterLepton[1];
      theLepton4 = iterLepton[1];
    }

    pzl1 = theLepton1->momentum().pz() ;
    thetal1 = theLepton1->momentum().theta();
    ptl1 = theLepton1->momentum().perp();
    pzl2 = theLepton2->momentum().pz() ;
    thetal2 = theLepton2->momentum().theta();
    ptl2 = theLepton2->momentum().perp();
    pzl3 = theLepton3->momentum().pz() ;
    thetal3 = theLepton3->momentum().theta();
    ptl3 = theLepton3->momentum().perp();
    pzl4 = theLepton4->momentum().pz() ;
    thetal4 = theLepton4->momentum().theta();
    ptl4 = theLepton4->momentum().perp();
  }else {
    // if number of leptons is lesser than expected, then skip this event
    thetal1= 100000;
    ptl1=-1000;
    pzl1=-1000;
    thetal2= 100000;
    ptl2=-1000;
    pzl2=-1000;
    thetal3= 100000;
    ptl3=-1000;
    pzl3=-1000;
    thetal4= 100000;
    ptl4=-1000;
    pzl4=-1000;
  }

  // lepton acceptance cuts: lhcb acc + pz > 0 + pt cut
  bool lep1_in_lhcbAcc = ( thetal1 <= m_thetaMax ) && ( thetal1 >= m_thetaMin ) && ( pzl1 >= 0. ) && ( ptl1 >= m_ptMin );
  bool lep2_in_lhcbAcc = ( thetal2 <= m_thetaMax ) && ( thetal2 >= m_thetaMin ) && ( pzl2 >= 0. ) && ( ptl2 >= m_ptMin );
  bool lep3_in_lhcbAcc = ( thetal3 <= m_thetaMax ) && ( thetal3 >= m_thetaMin ) && ( pzl3 >= 0. ) && ( ptl3 >= m_ptMin );
  bool lep4_in_lhcbAcc = ( thetal4 <= m_thetaMax ) && ( thetal4 >= m_thetaMin ) && ( pzl4 >= 0. ) && ( ptl4 >= m_ptMin );

  // different options 
  if((m_nBoson==-1 &&  m_nLepton ==0)// no cut
     ||(m_nBoson==-1 &&  m_nLepton ==2 && 
        ( lep1_in_lhcbAcc && lep2_in_lhcbAcc ) )
     // no boson cut, and 2 leptons
     ||(m_nBoson==-1 &&  m_nLepton ==3 && 
        ( lep1_in_lhcbAcc && lep2_in_lhcbAcc && lep3_in_lhcbAcc ) )
     // no boson cut, and 3 leptons
     ||(m_nBoson==-1 &&  m_nLepton ==4 && 
        ( lep1_in_lhcbAcc && lep2_in_lhcbAcc && lep3_in_lhcbAcc && lep4_in_lhcbAcc ) )
     // no boson cut, and 4 leptons
     ||(m_nBoson==1 &&  m_nLepton ==2 && 
	( BosonList.size() == 1 && lep1_in_lhcbAcc && lep2_in_lhcbAcc ) )
     // 1 boson, and 3 leptons
     ||(m_nBoson==1 &&  m_nLepton ==3 && 
	( BosonList.size() == 1 && lep1_in_lhcbAcc && lep2_in_lhcbAcc && lep3_in_lhcbAcc ) )
     // 1 boson, and 3 leptons
     ||(m_nBoson==1 &&  m_nLepton ==4 && 
	( BosonList.size() == 1 && lep1_in_lhcbAcc && lep2_in_lhcbAcc && lep3_in_lhcbAcc && lep4_in_lhcbAcc) )
     // 1 boson, and 4 leptons
     ||(m_nBoson==2 &&  m_nLepton ==2 && 
	( BosonList.size() == 2 && lep1_in_lhcbAcc && lep2_in_lhcbAcc ) )
     // 2 bosons, and 3 leptons
     ||(m_nBoson==2 &&  m_nLepton ==3 && 
	( BosonList.size() == 2 && lep1_in_lhcbAcc && lep2_in_lhcbAcc && lep3_in_lhcbAcc ) )
     // 2 bosons, and 3 leptons
     ||(m_nBoson==2 &&  m_nLepton ==4 && 
	( BosonList.size() == 2 && lep1_in_lhcbAcc && lep2_in_lhcbAcc && lep3_in_lhcbAcc && lep4_in_lhcbAcc) )
     // 2 bosons, and 4 leptons
     ) {
    if( msgLevel( MSG::DEBUG ) ){
      debug()  << "Event passed with requierement of "
               << m_nBoson << " boson, and " 
               << m_nLepton << " lepton." << endmsg ;
    }
    return true;

  } else {
    if( msgLevel( MSG::DEBUG ) ){
      debug() << "Event rejected with requierement of "<< m_nLepton
              << m_nBoson << " boson, and " 
              << m_nLepton << " lepton." << endmsg ;
    }
    return false;
  }
  return false ;
}
