/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files 
// ============================================================================
// STD& STL 
// ============================================================================
#include <map>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/ToStream.h"
// ============================================================================
// MCI-nterfaces 
// ============================================================================
#include "MCInterfaces/IDecayTool.h"
// ============================================================================
// GenEvent
// ============================================================================
#include "GenEvent/HepMCUtils.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/ParticleProperties.h"
#include "LoKi/GenParticleCuts.h"
#include "LoKi/GenExtract.h"
#include "LoKi/PrintHepMCDecay.h"
#include "LoKi/GenDecayChain.h"
#include "LoKi/ParticleProperties.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// Local
// ============================================================================
#include "GenericGenCutTool.h"
//=============================================================================
namespace LoKi 
{
  // ===========================================================================
  /** @class  GenCutToolWithDecay 
   *  
   *  Variant of LoKi::GenCutTool tool to be used for the special productions, 
   *  where the generator-level cuts and decay of the signal paricles are 
   *  combined in a single tool. Namely there are three types of such productions
   *  - production of Bc mesons with BcVegPy generator 
   *  - production of double heavy baryons  with GenXicc generator 
   *  - production of heavy quarkonia states (e.g. Upsilon-mesons)
   *
   *  @see LoKi::GenCutTool 
   *  In addition to LoKi::GenCutTool the tool have three additional 
   *  configurtaion parameters:
   *   - 'DecayTool'      with default value of <code>"EvtGenDecay"</code>
   *   - 'SignalParticle' with default value of <code>"<UNSPECIFIED>"</code>
   *   - 'PreDecay'       with default value of <code>true</code>
   *
   *  The value for <code>SignalParticle</code> *must* be specified. 
   *  The value of <code>PreDecay=true</code> is fine for the 
   *  excited Upsilon production, and actually  not needed for Bc and double heavy baryons, 
   *  where one can use <code>PreDecay=false</code>
   *
   *  Simple generic implementation of IGenCutTool interface to check if the 
   *  marked daughters are "good"
   *
   *  @code
   *  from Configurable import LoKi__GenCutToolWithDecay as CutTool 
   *  myalg.addTool ( CutTool , 'MyCutTool' ) 
   * 
   *  myAlg.MyCutTool.SignalPID      = "B_c+"  
   *  myAlg.MyCutTool.PreDecay       = False   
   *  myAlg.MyCutTool.Decay          = "^[B_c+ => ^(J/psi(1S) => ^mu+ ^mu-) ^pi+]CC" 
   *
   *  myAlg.MyCutTool.Cuts = {                             
   *   '[pi+]cc'   : " in_range ( 1.8 , GETA , 5.0 ) & ( GPT > 240 * MeV ) " ,
   *   '[mu+]cc'   : " in_range ( 1.8 , GETA , 5.0 ) & ( GPT > 540 * MeV ) " ,
   *   'J/psi(1S)' : " in_range ( 1.9 , GY   , 4.9 ) & ( GPT > 1   * GeV ) " ,
   *   '[B_c+]cc'  : " in_range ( 1.9 , GY   , 4.9 ) & ( GPT > 1.8 * GeV ) "
   *  }
   *  @endcode 
   *
   *  @see IGenCutTool
   *  @see LoKi::GenCutTool
   *
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   */
  class GenCutToolWithDecay : public LoKi::GenCutTool 
  {
    // ========================================================================
  public:
    // ========================================================================
    /// Initialization
    StatusCode initialize () override ;
    /// Finalization 
    StatusCode finalize   () override ;
    /** Accept events with 'good' particles.
     *  - first decay all particles heavier than signal particle  
     *  - decay the signal particle 
     *  - apply cuts 
     *  @see IGenCutTool::applyCut.
     */
    bool applyCut
    ( ParticleVector&           particles    , 
      const HepMC::GenEvent*    theEvent     , 
      const LHCb::GenCollision* theCollision ) const override ;
    // ========================================================================
  public:
    // ========================================================================
    /** standard constructor 
     *  @param type the tool type (???)
     *  @param name the actual instance name 
     *  @param parent the tool parent 
     */
    GenCutToolWithDecay 
    ( const std::string&  type   ,                     // the tool type (???)
      const std::string&  name   ,                     // the tool isntance name 
      const IInterface*   parent ) ;                   // the tool parent 
    /// virtual 
    virtual ~GenCutToolWithDecay () {}; // virtual desctructor 
    // ========================================================================
  public:
    // ========================================================================
    void updateSignal ( Gaudi::Details::PropertyBase& /* p */ ) ;
    // ========================================================================
  private:
    // ========================================================================
    /// the name/type of  the decay tool 
    std::string  m_decayToolName ; // the name/type of  the decay tool 
    /// the name of the particle to be decayed  
    std::string  m_particleName  ; // the name of the particle to be decayed  
    /// use  predecay   of heavy excited states ?
    bool         m_predecay      ;
    // ========================================================================
  private:
    // ========================================================================    
    /// The  decay tool itself 
    IDecayTool*       m_decayTool ; // The  decay tool itself 
    /// PDG ID of the particle to be decayed  
    LHCb::ParticleID  m_particle  ; // PDG ID of the particle to be decayed  
    // ========================================================================
    /// cut-off for m2 to consider the particle to be "heavy enough"
    double            m_heavy2    ; // "heavy-enough"
    // ========================================================================
    typedef std::map<LHCb::ParticleID,unsigned long long>  MAP1 ;
    typedef std::map<std::string     ,unsigned long long>  MAP2 ;
    mutable MAP2 m_decays        ;
    mutable MAP2 m_signal_decays ;
    // ========================================================================
  };
  // ==========================================================================
} //                                                  The end of namespace LoKi
// ============================================================================
namespace 
{
  // ==========================================================================
  // remove excessive blanks 
  inline std::string _process_ ( std::string s )
  {
    auto pos = s.find ("  ") ;
    while ( std::string::npos != pos ) { s.erase  ( pos     , 1 ) ; pos = s.find ( "  " ); }
    pos      = s.find ( "( " ) ;
    while ( std::string::npos != pos ) { s.erase  ( pos + 1 , 1 ) ; pos = s.find ( "( " ); }
    pos      = s.find ( " )" ) ;
    while ( std::string::npos != pos ) { s.erase  ( pos     , 1 ) ; pos = s.find ( " )" ); }
    return s ;
  }
  // ==========================================================================
} //                                             The end of anonymous namespace 
// ============================================================================
/*  standard constructor 
 *  @param type the tool type (???)
 *  @param name the actual instance name 
 *  @param parent the tool parent 
 */
// ============================================================================
LoKi::GenCutToolWithDecay::GenCutToolWithDecay 
( const std::string&  type   ,                     // the tool type (???)
  const std::string&  name   ,                     // the tool isntance name 
  const IInterface*   parent )                     // the tool parent 
  : LoKi::GenCutTool ( type , name , parent ) 
  , m_decayToolName  ( "EvtGenDecay"   ) 
  , m_particleName   ( "<UNPSPECIFIED>" ) // unspecified!!! 
  , m_predecay       ( true ) 
  , m_decayTool      ( 0    ) 
  , m_particle       ( 0    ) 
  , m_heavy2         ( 0    ) 
{
  declareProperty ( "DecayTool"      , 
                    m_decayToolName  , 
                    "DecayTool to be used"         ) ;
  declareProperty ( "SignalPID"      , 
                    m_particleName   , 
                    "The signal particle to decay" ) 
    -> declareUpdateHandler 
    ( &LoKi::GenCutToolWithDecay::updateSignal  , this ) ;
  declareProperty ( "PreDecay"       , 
                    m_predecay       , 
                    "Pre-decays of excited heavy non-signal states?" ) ;
  
}
// ============================================================================
// Initialization
// ============================================================================
StatusCode LoKi::GenCutToolWithDecay::initialize () 
{
  /// initialize the base 
  StatusCode sc = LoKi::GenCutTool::initialize() ;
  if ( sc.isFailure() ) { return sc ; }
  /// get decay tool
  m_decayTool = tool<IDecayTool>( m_decayToolName ) ;
  if ( !m_decayTool ) { return Error ( "Unable acquire decay tool '" + m_decayToolName + "'" ) ; }
  /// check the signal particle 
  //
  const LHCb::ParticleProperty* pp = LoKi::Particles::ppFromName ( m_particleName ) ;
  if ( 0 == pp ) { return  Error("Can't  fund properties for \"" + m_particleName + "\"" ) ; }
  m_particle = pp->particleID() ;
  if ( 0 >  m_particle.pid() ) { m_particle = LHCb::ParticleID ( m_particle.abspid() ) ; }
  ///
  const double mass  = pp -> mass  () ;
  const double width = std::max ( 0.0 , pp -> width () ) ;
  m_heavy2 = std::pow ( std::max ( 0.8 * mass , std::max ( 0.0 , mass - 5 * width ) ) , 2 ) ;
  ///  inform decay  tool  about the signal particle 
  m_decayTool -> setSignal ( m_particle.pid() ) ;
  ///
  return  StatusCode::SUCCESS ;
} 
// ============================================================================
// Finalization 
// ============================================================================
StatusCode LoKi::GenCutToolWithDecay::finalize () 
{
  if ( 0 != m_decayTool ) { releaseTool ( m_decayTool ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); m_decayTool = 0 ; }
  //
  typedef std::pair<unsigned long long, std::string> Item  ;
  typedef std::vector<Item>                          Items ;
  //
  if ( !m_decays.empty() ) 
  {
    Items items ;
    info () << "Heavy particles (pre)decayed (" << m_decays.size() << "):" << endmsg ;
    unsigned int maxlen = 0 ;    
    for ( const auto& item : m_decays ) 
    {
      unsigned int len = item.first.size() ;
      maxlen = std::max ( maxlen , len ) ;
      items.push_back ( std::make_pair ( item.second , item.first ) ) ;
    }
    std::stable_sort ( items.begin () , items.end () ) ;
    std::reverse     ( items.begin () , items.end () ) ;
    std::string strfmt = " %|9d| : %|-" + Gaudi::Utils::toString ( maxlen + 2 ) + "s|" ;
    for ( const auto& item : items  ) 
    {
      boost::format fmt ( strfmt ) ;
      fmt  % item.first % item.second ;
      info() << fmt.str() << endmsg ;
    }    
  }
  // ==========================================================================
  if ( !m_signal_decays.empty() ) 
  {
    Items items ;
    info () << "Signals decays (" << m_signal_decays.size() << "):" << endmsg ;
    unsigned int maxlen = 0 ;
    for ( const auto& item : m_signal_decays ) 
    {
      unsigned int len = item.first.size() ;
      maxlen = std::max ( maxlen , len ) ;
      items.push_back ( std::make_pair ( item.second , item.first ) ) ;
    }
    std::stable_sort ( items.begin () , items.end () ) ;
    std::reverse     ( items.begin () , items.end () ) ;
    std::string strfmt = " %|9d| : %|-" + Gaudi::Utils::toString ( maxlen + 2 ) + "s|" ;
    for ( const auto& item : items ) 
    {
      boost::format fmt ( strfmt ) ;
      fmt  % item.first % item.second ;
      info() << fmt.str() << endmsg ;
    }    
  }
  //
  return  LoKi::GenCutTool::finalize () ;   
}
// ============================================================================
/*  Accept events with 'good' particles.
 *  - first decay all particles heavier than signal particle  
 *  - decay the signal particle 
 *  - apply cuts 
 *  @see IGenCutTool::applyCut.
 */
// ============================================================================
bool LoKi::GenCutToolWithDecay::applyCut
( ParticleVector&              particles    , 
  const HepMC::GenEvent*       theEvent     , 
  const LHCb::GenCollision*    theCollision ) const 
{
  m_decayTool -> disableFlip() ;
  // ==========================================================================
  // 1) First pre-decay all non-signal particles (heavier than the signal)
  // ==========================================================================
  if ( m_predecay ) 
  {
    HepMCUtils::ParticleSet for_decays ;  
    for ( HepMC::GenEvent::particle_const_iterator it = theEvent -> particles_begin() ; 
          theEvent -> particles_end() != it ; ++it )
    {
      HepMC::GenParticle* h = *it ;
      if ( 0 == h ) { continue ; } 
      if ( LHCb::HepMCEvent::DocumentationParticle == h->status() ) { continue ; }
      if ( LHCb::HepMCEvent::StableInProdGen       != h->status() ) { continue ; }    
      if ( m_particle.pid() == std::abs ( h->pdg_id()  )          ) { continue ; }
      if ( m_heavy2 < h->momentum().m2() && m_decayTool ->isKnownToDecayTool ( h->pdg_id() ) ) 
      { for_decays.insert ( h ) ; }
    }
    // decay them
    for ( HepMC::GenParticle*  p : for_decays ) 
    { 
      if ( nullptr == p )  { continue ; }
      const LHCb::ParticleID pid{ p->pdg_id() } ; 
      m_decayTool -> generateDecayWithLimit ( p , m_particle.pid() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); 
      if ( msgLevel ( MSG::DEBUG ) )
      { ++m_decays[ LoKi::Particles::nameFromPID ( pid )  ] ; }
    }
  }
  // =================================================================================
  // 2) select all signal decays 
  // =================================================================================
  particles.clear() ; 
  using namespace LoKi::Cuts ;
  LoKi::Extract::getGenParticles 
    ( theEvent                        , 
      std::back_inserter( particles ) , 
      ( m_particle.pid()                  == GABSID  ) && 
      ( LHCb::HepMCEvent::StableInProdGen == GSTATUS ) ) ;
  //
  if      ( 1 < particles.size() )   // too many signals ? 
  { 
    Warning ( "Too many signals are found, skip extra").ignore() ; 
    particles.erase ( particles.begin() + 1 , particles.end() ) ;       // ERASE!!
  }
  else if ( particles.empty() )      // no signals? 
  {
    Warning ( "No signal decays are found", StatusCode::FAILURE , 0 ).ignore() ;
    return false ;                                                // RETURN
  }
  // choose the signal as the first particle 
  HepMC::GenParticle* signal = particles.front() ;
  // ==========================================================================
  // make decay of signal particle 
  // ==========================================================================
  bool hasFlipped = false ;
  StatusCode sc = m_decayTool -> generateSignalDecay ( signal , hasFlipped ) ;
  auto EV = signal->end_vertex();
  if(EV){
    signal->parent_event()->set_signal_process_vertex(EV);
  }
  if ( sc.isFailure() ) 
  {
    Error( "Error from decay of the signal particle, skip event" , sc ).ignore() ;
    return false ;
  }
  ///
  if ( msgLevel ( MSG::DEBUG ) )
  { ++m_signal_decays [ _process_ ( LoKi::PrintHepMC::printDecay ( signal ) )  ] ; }
  //
  // and now apply cuts for signals 
  return  LoKi::GenCutTool::applyCut ( particles , theEvent , theCollision ) ;  
}
// ============================================================================
// update-handler for the property "SignalPID" 
// =============================================================================
void LoKi::GenCutToolWithDecay::updateSignal  ( Gaudi::Details::PropertyBase& /* p */ ) 
{
  // no action if not yet initialized 
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return ; }
  //
  const LHCb::ParticleProperty* pp = LoKi::Particles::ppFromName ( m_particleName ) ;
  Assert  ( 0 != pp , "Can't  fund properties for \"" + m_particleName + "\"" ) ;
  m_particle = pp->particleID() ;
  if ( 0 >  m_particle.pid() ) { m_particle = LHCb::ParticleID ( m_particle.abspid() ) ; }
  ///
  const double mass  = pp -> mass  () ;
  const double width = std::max ( 0.0 , pp -> width () ) ;
  m_heavy2 = std::pow ( std::max ( 0.8 * mass , std::max ( 0.0 , mass - 5 * width ) ) , 2 ) ;
  ///  inform decay  tool  about the signal particle 
  m_decayTool -> setSignal ( m_particle.pid() ) ;
  // ==========================================================================
}
// ============================================================================
// factory
// ============================================================================
DECLARE_COMPONENT(LoKi::GenCutToolWithDecay) 
// ============================================================================
//                                                                      The END 
// ============================================================================


  

