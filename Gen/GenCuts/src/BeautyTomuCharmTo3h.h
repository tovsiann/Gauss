/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_BEAUTYTOMUCHARMTO3H_H
#define GENCUTS_BEAUTYTOMUCHARMTO3H_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"

#include "MCInterfaces/IFullGenEventCutTool.h"
#include "MCInterfaces/IGenCutTool.h"
#include "GaudiKernel/Transform4DTypes.h"

/** @class BeautyTomuCharmTo3h BeautyTomuCharmTo3h
 *
 *  Tool to filter SL decays of H_b->H_c(*)(3h)munu and H_b->H_c(*)(3h)tau(mununu)nu decays
 *  while only enforcing that the 3h from the H_c decay and the mu from the H_b or tau
 *  are in the acceptance.
 *  Allows for the use of kinematic requirements (p/pT) on the muon and hadrons.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Stephen Ogilvy
 *  @date   2017-03-17
 */

class BeautyTomuCharmTo3h: public GaudiTool, virtual public IGenCutTool {
 public:
  /// Standard constructor
  BeautyTomuCharmTo3h( const std::string& type,
                       const std::string& name,
                       const IInterface* parent);

  virtual ~BeautyTomuCharmTo3h( ); // Destructor

  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:

  // Function to check the muon passes the acceptance cuts.
  void passMuonKinematics(const HepMC::GenParticle * theMuon,
                          bool &hasMuon) const ;

  // Function to check the muon passes the acceptance cuts.
  void passMuonFromTauonKinematics(const HepMC::GenParticle * theTauon,
                                   bool &hasMuon) const ;

  // Function to check a Ds has all daughters accepted.
  void passCharmTo3hKinematics(const HepMC::GenParticle * theDs,
                               bool &hasHadrons) const ;

  double m_chargedThetaMin ;
  double m_chargedThetaMax ;
  double m_muonptmin;
  double m_muonpmin;
  double m_hadronptmin;
  double m_hadronpmin;

};
#endif // GENCUTS_BEAUTYTOMUCHARMTO3H_H
