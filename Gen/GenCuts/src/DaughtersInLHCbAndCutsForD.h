/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORDS_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORD_H 1

// Include files
#include "DaughtersInLHCb.h"

/** @class DaughtersInLHCbAndCutsForD DaughtersInLHCbAndCutsForD.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb and with p and pt cuts on D daughters.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Benedetto G. Siddi
 *  @date   2020-03-27
 */
class DaughtersInLHCbAndCutsForD : public DaughtersInLHCb, virtual public IGenCutTool {
 public:
  /// Standard constructor
  DaughtersInLHCbAndCutsForD( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent);

  virtual ~DaughtersInLHCbAndCutsForD( ); ///< Destructor

  /** Accept events with daughters in LHCb and p/pt cuts on Dstar daughters
   *  (defined by min and max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  virtual bool applyCut( ParticleVector & theParticleVector ,
                         const HepMC::GenEvent * theEvent ,
                         const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndWithMinP
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  // cut value of D(s) pt
  double m_dptCut ;
  
  // cut on D(s) ctau
  double m_dctauCut ;
  
  // cut value on daughters min pt
  double m_daughtersptminCut ;

  // cut value on daughters max pt
  double m_daughtersptmaxCut ;

  // cut value on daughters min p
  double m_daughterspminCut ;
  
  int m_signalID;

  std::vector<int> m_childs;
};
#endif // GENCUTS_DAUGHTERSINLHCDANDCUTSFORD_H
