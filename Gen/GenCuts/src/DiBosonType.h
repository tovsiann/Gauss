/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DiBosonType_H
#define GENCUTS_DiBosonType_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IParticlePropertySvc.h"

// from Generators
#include "MCInterfaces/IGenCutTool.h"

/** @class DiBosonType DiBosonType.h 
 *
 *  Tool to select events with, three or four leptons from di-boson decay 
 *  in the acceptance, with given minimal pt and comming (or not) from
 *  a W or Z boson
 *
 *  @author Hang Yin
 *  @date   2020-11-18
 */
class DiBosonType : public GaudiTool , virtual public IGenCutTool {
public:
  /// Standard constructor
  DiBosonType( const std::string & type , const std::string & name ,
	       const IInterface * parent ) ;

  virtual ~DiBosonType( ); ///< Destructor

  /// initialize function
  StatusCode initialize( ) override;

  /* Accept events following criteria given in options.
   * Implements IGenCutTool::applyCut
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

private:
  /// Return true if the particle is a lepton
  bool IsLepton( const HepMC::GenParticle * p ) const ;

  /// Return true if the particle is a EW boson
  bool IsBoson( const HepMC::GenParticle * p ) const ;

  /// Max and Min theta angle to define the LHCb acceptance
  double m_thetaMax ;
  double m_thetaMin ;

  /// Number of leptons required in the acceptance
  int m_nLepton;

  /// Number of bosons 
  int m_nBoson;

  /// Type of lepton requiered in the acceptance
  std::vector< std::string > m_TypeLepton;

  /// Minimum pT of the lepton
  double m_ptMin ;

  /// If true, the lepton is required to come from a given mother
  bool m_leptonFromMother;

  /// If true, the boson is required to come from a given mother
  bool m_bosonFromMother;

  /// List of mother of the lepton
  std::vector< std::string > m_motheroflepton{"W+","Z0"};

  /// Minimum mass of the Mother of the lepton
  double m_MinMass ;

  /// PDG id of the mother of the W/Z boson 
  std::string m_motherofboson_id;
  int m_motherofboson_pid;

  LHCb::IParticlePropertySvc* m_ppSvc;
};
#endif // GENCUTS_DiBosonType_H
