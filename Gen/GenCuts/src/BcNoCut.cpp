/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BcNoCut.cpp,v 1.4 2009-10-02 13:08:08 jhe Exp $
// Include files 

// local
#include "BcNoCut.h"

// from Gaudi
#include "GaudiKernel/Vector4DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Kernel
#include "Kernel/ParticleID.h"

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

// from Generators
#include "GenEvent/HepMCUtils.h"
#include "MCInterfaces/IDecayTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : BcNoCut
// 
// 2013-06-11 : Jibo He
// 2005-08-17 : Patrick Robbe
// 2020-12-02 : Liupan An (changed in accordance to BcVegPy update to include excited Bc states)
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( BcNoCut )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BcNoCut::BcNoCut( const std::string& type,
                  const std::string& name,
                  const IInterface* parent )
  : GaudiTool ( type, name , parent ),
    m_decayTool( 0 ) {
  declareInterface< IGenCutTool >( this ) ;
  declareProperty( "DecayTool" ,       m_decayToolName = "EvtGenDecay") ;
  m_sigBcPID = 541 ;

}

//=============================================================================
// Destructor 
//=============================================================================
BcNoCut::~BcNoCut( ) { ; }

//=============================================================================
// Initialize
//=============================================================================
StatusCode BcNoCut::initialize( ) {

  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiTool

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize and retrieve "
                                      << m_decayToolName << " tool" << endmsg;

  if ( "" != m_decayToolName )
    m_decayTool = tool< IDecayTool >( m_decayToolName ) ;

  m_decayTool -> setSignal( m_sigBcPID ) ;

  return StatusCode::SUCCESS;

}

//=============================================================================
// Finalize
//=============================================================================
StatusCode BcNoCut::finalize( ) {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  if ( 0 != m_decayTool ) release( m_decayTool ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  return GaudiTool::finalize(); 

}

//=============================================================================
// Acceptance function
//=============================================================================
bool BcNoCut::applyCut( ParticleVector & theParticleVector ,
                        const HepMC::GenEvent * theEvent  ,
                        const LHCb::GenCollision */* theHardInfo */ )
  const {
  // First decay all particles heavier than the Bc
  m_decayTool -> disableFlip() ;
  HepMCUtils::ParticleSet particleSet ;  
  HepMC::GenEvent::particle_const_iterator it ;
  for ( it = theEvent -> particles_begin() ; 
        it != theEvent -> particles_end() ; ++it )
     if ( LHCb::ParticleID( (*it) -> pdg_id() ).hasQuark( LHCb::ParticleID::bottom ) ) 
        particleSet.insert( *it ) ;

  for ( HepMCUtils::ParticleSet::iterator itHeavy = particleSet.begin() ; 
        itHeavy != particleSet.end() ; ++itHeavy ) 

     if ( ( LHCb::HepMCEvent::StableInProdGen == (*itHeavy) -> status() ) && 
           ( m_sigBcPID != abs( (*itHeavy) -> pdg_id() ) ) ) {

        if ( m_decayTool -> isKnownToDecayTool( (*itHeavy) -> pdg_id() ) ) 
           m_decayTool -> generateDecayWithLimit( *itHeavy , m_sigBcPID ).ignore() ;
     }
  
  
  // To see whether the B_c is in the Event or not
  //--------------------------------------------------------------------
  theParticleVector.clear( ) ;
  for ( it = theEvent -> particles_begin() ; 
        it != theEvent -> particles_end() ; ++it )
    if ( abs( (*it) -> pdg_id() ) == m_sigBcPID ) 
      if ( ( LHCb::HepMCEvent::DocumentationParticle != (*it) -> status() ) 
           && ( HepMCUtils::IsBAtProduction( *it ) ) )
        theParticleVector.push_back( *it ) ;
  std::sort( theParticleVector.begin() , theParticleVector.end() , 
             HepMCUtils::compareHepMCParticles ) ;
  
  if ( theParticleVector.empty() ) return false ;

  // To decay the signal particle
  //--------------------------------------------------------------------  
  bool hasFlipped = false ;
  HepMC::GenParticle * theSignal ;
  theSignal = theParticleVector.front() ;  
  m_decayTool -> generateSignalDecay( theSignal , hasFlipped ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  auto EV = theSignal->end_vertex();
  if(EV){
    theSignal->parent_event()->set_signal_process_vertex(EV);
  }
  
  return ( ! theParticleVector.empty() ) ;
}
