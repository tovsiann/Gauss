/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "DaughtersInLHCbAndCutsForD.h"

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Transform4DTypes.h"

// from Kernel
#include "Kernel/ParticleID.h"
#include "GaudiKernel/Vector4DTypes.h"

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

// from Generators
#include "GenEvent/HepMCUtils.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndCutsForD
//
// 2020-03-27 : Benedetto G. Siddi
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCbAndCutsForD )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DaughtersInLHCbAndCutsForD::DaughtersInLHCbAndCutsForD( const std::string& type,
                                                          const std::string& name,
                                                          const IInterface* parent )
  : DaughtersInLHCb ( type, name , parent ) {
  declareInterface< IGenCutTool >( this ) ;
  declareProperty( "DPtCuts"          , m_dptCut           = 1700 * Gaudi::Units::MeV ) ;
  declareProperty( "DaughtersPtMinCut" , m_daughtersptminCut = 700 * Gaudi::Units::MeV ) ;
  declareProperty( "DaughtersPtMaxCut" , m_daughtersptmaxCut = 1050 * Gaudi::Units::MeV ) ;
  declareProperty( "DaughtersPMinCut"  , m_daughterspminCut  = 4500 * Gaudi::Units::MeV ) ;
  declareProperty( "DCTauCut"         , m_dctauCut         = -1. ) ;
  declareProperty( "SignalID"          , m_signalID          = 431);
  declareProperty( "Childrens"         , m_childs            = {321, 211});
}

//=============================================================================
// Destructor
//=============================================================================
DaughtersInLHCbAndCutsForD::~DaughtersInLHCbAndCutsForD( ) { ; }

//=============================================================================
// AndWithMinP function
//=============================================================================
bool DaughtersInLHCbAndCutsForD::applyCut( ParticleVector & theParticleVector ,
                                               const HepMC::GenEvent * theEvent ,
                                               const LHCb::GenCollision * theHardInfo )
  const {
  if( ! DaughtersInLHCb::applyCut( theParticleVector, theEvent, theHardInfo ) ) {
    return false;
  }
  ParticleVector::iterator it ;

  for ( it = theParticleVector.begin() ; it != theParticleVector.end() ; ) {
    // Check it is a D(s)+/-
    
    if ( abs( (*it) -> pdg_id() ) != m_signalID )
      Exception( "The signal is not a D(s)+ or D(s)-" ) ;
    
    if ( ! passCuts( *it ) ) {
      it = theParticleVector.erase( it ) ;
    } else ++it ;
  }

  return ( ! theParticleVector.empty() ) ;
}

//=============================================================================
// Functions to test if all daughters are in acceptance
//=============================================================================
bool DaughtersInLHCbAndCutsForD::passCuts( const HepMC::GenParticle * theSignal ) const {
  HepMC::GenVertex * EV = theSignal -> end_vertex() ;
  if ( 0 == EV ) return true ;

  typedef std::vector< HepMC::GenParticle * > Particles ;
  Particles dsdaughters ;

  HepMC::GenVertex::particle_iterator iter ;

  for ( iter = EV -> particles_begin( HepMC::descendants ) ;
        iter != EV -> particles_end( HepMC::descendants ) ; ++iter ) {
       // Fill all daughters but exclude photons (can be radiative photons)
    int pdgchild = (*iter) -> pdg_id();
    if ( 0 == (*iter) -> end_vertex() )
      if ( 22 != (*iter) -> pdg_id() ){
        if(!m_childs.empty()){
          std::vector<int>::const_iterator it = std::find(m_childs.begin(), m_childs.end(), abs(pdgchild));
          if (it != m_childs.end()){
            dsdaughters.push_back(*iter);
          }
        }
        else
          dsdaughters.push_back(*iter);
      }
  }
  if ( dsdaughters.empty() )
    Exception( "No D(s) daughters in signal chain !" );


  if ( msgLevel( MSG::DEBUG ) ) debug() << "New event" << endmsg ;

  // Now check other cuts
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Check other cuts" << endmsg ;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "D(s) pT = " << theSignal -> momentum().perp() << endmsg ;
  if ( theSignal -> momentum().perp() < m_dptCut ) return false ;

  if ( m_dctauCut > 0. ) {
    // Apply ctau cut
    const HepMC::FourVector& Dfourmomentum = theSignal -> momentum();
    double gamma = Dfourmomentum . e() / Dfourmomentum . m() ;
    double delta_t = theSignal -> end_vertex() -> position() . t() - theSignal -> production_vertex() -> position() . t() ;
    double ctau = Gaudi::Units::c_light * delta_t / gamma ;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "D(s) ctau = " << ctau << endmsg ;
    if ( ctau < m_dctauCut ) return false ;
  }

  double minpt = 14.*Gaudi::Units::TeV ;
  double maxpt = 0. ;
  double minp  = 14.*Gaudi::Units::TeV ;

  for ( Particles::const_iterator it = dsdaughters.begin() ; it != dsdaughters.end() ;
        ++it ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Daughter pT = " << (*it) -> momentum().perp()
              << " p = " << (*it) -> momentum().rho() << endmsg ;
    }
    if ( (*it) -> momentum().perp() > maxpt )
      maxpt = (*it) -> momentum().perp() ;
    if ( (*it) -> momentum().perp() < minpt )
      minpt = (*it) -> momentum().perp() ;
    if ( (*it) -> momentum().rho() < minp )
      minp = (*it) -> momentum().rho() ;
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Min Pt = " << minpt
            << " Max Pt = " << maxpt
            << " Min P = " << minp << endmsg ;
  }

  if ( minpt < m_daughtersptminCut ) return false ;
  if ( maxpt < m_daughtersptmaxCut ) return false ;
  if ( minp  < m_daughterspminCut  ) return false ;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Event passed !" << endmsg ;

  return true ;
}

