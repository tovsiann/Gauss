/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#if 0
*       for VAX
* This pilot patch was created from kernvax.car patch _kalph
* This directory was created from kernvax.car patch vaxalpha
* This directory was created from kernvax.car patch qmvax
* This directory was created from kernfor.car patch qmvax
*               CC assumed available
*     external names without underscores
*     ISA standard routines, ISHFT, IOR, etc
*     MIL standard routines, IBITS, MVBITS, ISHFTC
*       Hollerith constants exist
*    EQUIVALENCE Hollerith/Character ok
*       Hollerith storage not orthodox
#endif
#ifndef CERNLIB_QMALPH
#define CERNLIB_QMALPH
#endif
#ifndef CERNLIB_QMVAXCC
#define CERNLIB_QMVAXCC
#endif
#ifndef CERNLIB_QMVAX
#define CERNLIB_QMVAX
#endif
#ifndef CERNLIB_QXNO_SC
#define CERNLIB_QXNO_SC
#endif
#ifndef CERNLIB_QISASTD
#define CERNLIB_QISASTD
#endif
#ifndef CERNLIB_QMILSTD
#define CERNLIB_QMILSTD
#endif
#ifdef CERNLIB_QORTHOLL
#undef CERNLIB_QORTHOLL
#endif
#ifndef CERNLIB_QINTZERO
#define CERNLIB_QINTZERO
#endif
