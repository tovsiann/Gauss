/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBCRMC_BOOSTFOREPOS_H
#define LBCRMC_BOOSTFOREPOS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/Transform4DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"

/** @class BoostForEpos BoostForEpos.h
 *
 *
 *  @author Patrick Robbe
 *  @date   2016-10-21
 */
class BoostForEpos : public GaudiAlgorithm {
public:
  /// Standard constructor
  BoostForEpos( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~BoostForEpos( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  std::string m_inputHepMCEvent  ;
  double m_px, m_py, m_pz , m_e ;
  ROOT::Math::Boost m_boost ;
};
#endif // LBCRMC_BOOSTFOREPOS_H
