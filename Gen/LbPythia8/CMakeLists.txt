###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LbPythia8
################################################################################
gaudi_subdir(LbPythia8 v12r0)

gaudi_depends_on_subdirs(Gen/Generators)

find_package(Pythia8 COMPONENTS pythia8 pythia8tohepmc)
find_package(HepMC COMPONENTS fio)
find_package(Boost)

if(EXISTS "${PYTHIA8_INCLUDE_DIR}/Pythia8/Pythia.h")
  # include directory changed from Pythia8 175 to 183
  include_directories(SYSTEM ${PYTHIA8_INCLUDE_DIR}/Pythia8)
endif()

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS} ${PYTHIA8_INCLUDE_DIRS})

gaudi_add_library(LbPythia8Lib
                  src/Lib/*.cpp
                  PUBLIC_HEADERS LbPythia8
                  INCLUDE_DIRS Pythia8
                  LINK_LIBRARIES GeneratorsLib Pythia8)

gaudi_add_module(LbPythia8
                 src/component/*.cpp
                 LINK_LIBRARIES GeneratorsLib LbPythia8Lib HepMC)

gaudi_env(SET PYTHIA8XML ${PYTHIA8_XML})

