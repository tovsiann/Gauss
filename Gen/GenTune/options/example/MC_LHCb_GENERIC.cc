/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// -*- C++ -*-
#include <Rivet/Analysis.hh>
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/ChargedFinalState.hh"

using namespace std;

namespace Rivet {


  /// Generic analysis looking at various distributions of final state particles
  class MC_LHCb_GENERIC : public Analysis {
  public:

    /// Constructor
    MC_LHCb_GENERIC()
      : Analysis("MC_LHCb_GENERIC"),
        nerrinf(0), nerrnan(0)
    { 
       nphotons = 0;
       npions = 0;
       minPhtPx = 0.;
       maxPhtPx = 0.;
       minPhtPz = 0.;
       maxPhtPz = 0.;
       minPi0Px = 0.;
       maxPi0Px = 0.;
       minPi0Pz = 0.;
       maxPi0Pz = 0.;
       minPhtM = 0.; maxPhtM = 0.;
       minPi0M = 0.; maxPi0M = 0.;
    }


  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      MSG_INFO("Initializing analysis module " << this->name() << "...");
      // Projections
      const FinalState cnfs(Cuts::eta > -10.0 && Cuts::eta < 10.0 && Cuts::pT > 150*MeV);
      declare(UnstableParticles(Cuts::eta > -10.0 && Cuts::eta < 10.0 && Cuts::pT > 100*MeV), "UFS");
      declare(cnfs, "FS");
      declare(ChargedFinalState(Cuts::eta > -10.0 && Cuts::eta < 10.0 && Cuts::pT > 150*MeV), "CFS");

      // Histograms
      // @todo Choose E/pT ranged based on input energies... can't do anything about kin. cuts, though
      book(_histMult, "Mult", 100, -0.5, 199.5);
      book(_histMultCh, "MultCh", 100, -0.5, 199.5);
      MSG_INFO("Booked multiplicity histos...");

      book(_histPt, "Pt", 300, 0, 30);
      book(_histPtCh, "PtCh", 300, 0, 30);

      book(_histPx, "Px", 1000, -50, 50);
      book(_histPy, "Py", 1000, -50, 50);

      book(_hPhtPx, "PhtPxM", 40, -70, 330, 40, -4.1, 3.9);
      book(_hPhtPz, "PhtPzM", 40, -70, 330, 120, -605, 595);
      book(_hPi0Px, "Pi0PxM", 50, -116, 159, 40, -4.1, 3.9);
      book(_hPi0Pz, "Pi0PzM", linspace(100, -366, 634), linspace(200, -3417, 3383));

      book(_histE, "E", 100, 0, 200);
      book(_histECh, "ECh", 100, 0, 200);
      MSG_INFO("Booked momenta and energy histos...");

      book(_histEta, "Eta", 100, -10, 10);
      book(_histEtaCh, "EtaCh", 100, -10, 10);
      _tmphistEtaPlus = Histo1D(50, 0, 10);
      _tmphistEtaMinus = Histo1D(50, 0, 10);
      _tmphistEtaChPlus = Histo1D(50, 0, 10);
      _tmphistEtaChMinus = Histo1D(50, 0, 10);
      book(_hEtaRatio, "EtaPMRatio"); //, 50, 0., 10.);
      book(_hEtaChRatio, "EtaChPMRatio"); //, 50, 0., 10.);

      book(_histEtaSumEt, "EtaSumEt", 50, 0, 10);
      MSG_INFO("Booked pseudorapidity histos...");

      book(_histRapidity, "Rapidity", 100, -10, 10);
      book(_histRapidityCh, "RapidityCh", 100, -10, 10);
      _tmphistRapPlus = Histo1D(50, 0, 10);
      _tmphistRapMinus = Histo1D(50, 0, 10);
      _tmphistRapChPlus = Histo1D(50, 0, 10);
      _tmphistRapChMinus = Histo1D(50, 0, 10);
      book(_hRapRatio, "RapidityPMRatio"); //, 50, 0, 10);
      book(_hRapChRatio, "RapidityChPMRatio"); //, 50, 0, 10);

      book(_histPhi ,"Phi", 50, 0, TWOPI);
      book(_histPhiCh, "PhiCh", 50, 0, TWOPI);
      MSG_INFO("Booked particle geometry histos (etarap, rap, phi) ...");
    }



    /// Perform the per-event analysis
    void analyze(const Event& event) {
      // const double weight = event.weight();
      double mm, px, py, pz, pT;
      // Iterate through unstable particle list
      const UnstableParticles& ufs = applyProjection<UnstableParticles>(event, "UFS");
      for (const Particle &p : ufs.particles()) {
        if ( p.pid() == 111 ) {
        	px = p.px()/GeV;
        	pz = p.pz()/GeV;
        	mm = p.mass()/MeV;
          _hPi0Px->fill(mm, px);
          _hPi0Pz->fill(mm, pz);
          npions ++;
          if (minPi0Px > px) minPi0Px = px;
          if (maxPi0Px < px) maxPi0Px = px;
          if (minPi0Pz > pz) minPi0Pz = pz;
          if (maxPi0Pz < pz) maxPi0Pz = pz;
          if (minPi0M > mm) minPi0M = mm;
          if (maxPi0M < mm) maxPi0M = mm;
        };
      }

      // Charged + neutral final state
      const FinalState& cnfs = applyProjection<FinalState>(event, "FS");
      MSG_DEBUG("Total multiplicity = " << cnfs.particles().size());
      _histMult->fill(cnfs.particles().size());
      for (const Particle& p : cnfs.particles()) {
        const double eta = p.eta();
        const double pet = p.Et()/GeV;
        MSG_DEBUG("Filling eta!");
        _histEta->fill(eta);
        MSG_DEBUG("Filling eta sum E_T!");
        _histEtaSumEt->fill(fabs(eta), pet);
        if (eta > 0) {
          MSG_DEBUG("Filling eta plus!");
          _tmphistEtaPlus.fill(fabs(eta));
        } else {
          MSG_DEBUG("Filling eta minus!");
          _tmphistEtaMinus.fill(fabs(eta));
        }
        const double rapidity = p.rapidity();
        MSG_DEBUG("Filling rapidity (" << std::setw(6) << std::fixed << rapidity << std::scientific << ")!");
        if (isinf(rapidity)) {
          if (nerrinf < 100) MSG_ERROR("Rapidity is Inf for particle " << p.pid() << " (HEPMC status=" << p.genParticle()->status()  << ")...");
          nerrinf ++;
          continue;
        };
        if (isnan(rapidity)) {
          if (nerrnan < 100) MSG_ERROR("Rapidity is NaN for particle " << p.pid() << " (status=" << p.genParticle()->status() << ")...");
          nerrnan ++;
          continue;
        };
        _histRapidity->fill(rapidity);
        if (rapidity > 0) {
          MSG_DEBUG("Filling rapidity plus!");
          _tmphistRapPlus.fill(rapidity);
        } else {
          MSG_DEBUG("Filling rapidity minus!");
          _tmphistRapMinus.fill(-rapidity);
        }
        if ((&p) == NULL) {MSG_ERROR("Null particle reference!"); continue;};
        px = p.px()/GeV;
        py = p.py()/GeV;
        pT = p.pT()/GeV;
        MSG_DEBUG("px = " << px << " GeV/c");
        MSG_DEBUG("py = " << py << " GeV/c");
        //if ((&Histo1DPtr::fill) == NULL) {MSG_ERROR("Null fill reference!"); continue;};
        MSG_DEBUG("Filling px!");
        _histPx->fill(px);
        MSG_DEBUG("Filling py!");
        _histPy->fill(py);
        MSG_DEBUG("Filling pT!");
        _histPt->fill(p.pT()/GeV);
        MSG_DEBUG("Filling E!");
        _histE->fill(p.E()/GeV);
        MSG_DEBUG("Filling phi!");
        _histPhi->fill(p.phi());
        if ( p.pid() == 22 ) {
        	pz = p.pz()/GeV;
        	mm = p.mass()/MeV;
          _hPhtPx->fill(mm, px);
          _hPhtPz->fill(mm, pz);
          nphotons ++;
          if (minPhtPx > px) minPhtPx = px;
          if (maxPhtPx < px) maxPhtPx = px;
          if (minPhtPz > pz) minPhtPz = pz;
          if (maxPhtPz < pz) maxPhtPz = px;
          if (minPhtM > mm) minPhtM = mm;
          if (maxPhtM < mm) maxPhtM = mm;
        };
      }
      const ChargedFinalState& cfs = applyProjection<ChargedFinalState>(event, "CFS");
      MSG_DEBUG("Total charged multiplicity = " << cfs.particles().size());
      _histMultCh->fill(cfs.particles().size());
      for (const Particle& p : cfs.particles()) {
        const double eta = p.pseudorapidity();
        pT = p.pT()/GeV;
        _histEtaCh->fill(eta);
        if (eta > 0.) {
          _tmphistEtaChPlus.fill(eta);
        } else {
          _tmphistEtaChMinus.fill(-eta);
        }
        const double rapidity = p.momentum().rapidity();
        if (isnan(rapidity) || isinf(rapidity)) continue; //avoid events with bad data!
        _histRapidityCh->fill(rapidity);
        if (rapidity > 0.) {
          _tmphistRapChPlus.fill(rapidity);
        } else {
          _tmphistRapChMinus.fill(-rapidity);
        }
        _histPtCh->fill(pT);
        _histECh->fill(p.E()/GeV);
        _histPhiCh->fill(p.phi());
      }


    }



    /// Finalize
    void finalize() {
    	// make qmtest not fail!
    	if (_hPhtPx->effNumEntries(false) < 1.e-9) _hPhtPx->fill(-65.0, -4.0);
    	if (_hPhtPz->effNumEntries(false) < 1.e-9) _hPhtPz->fill(-65.0, -600.0);
    	if (_hPi0Px->effNumEntries(false) < 1.e-9) _hPi0Px->fill(-110.0, -4.0);
    	if (_hPi0Pz->effNumEntries(false) < 1.e-9) _hPi0Pz->fill(-364.0, -3400.0);


      scale(_histMult, 1./sumOfWeights());
      scale(_histMultCh, 1./sumOfWeights());

      scale(_histEta, 1./sumOfWeights());
      scale(_histEtaCh, 1./sumOfWeights());

      scale(_histRapidity, 1./sumOfWeights());
      scale(_histRapidityCh, 1./sumOfWeights());

      scale(_histPt, 1./sumOfWeights());
      scale(_histPtCh, 1./sumOfWeights());

      scale(_histPx, 1./sumOfWeights());
      scale(_histPy, 1./sumOfWeights());

      scale(_histE, 1./sumOfWeights());
      scale(_histECh, 1./sumOfWeights());

      scale(_histPhi, 1./sumOfWeights());
      scale(_histPhiCh, 1./sumOfWeights());

      // scale to get medium multiplicity per event
      /* scale(_hPhtPx,1./sumOfWeights());
      scale(_hPhtPz,1./sumOfWeights());
      scale(_hPi0Px,1./sumOfWeights());
      scale(_hPi0Pz,1./sumOfWeights()); */

      divide(_tmphistEtaPlus, _tmphistEtaMinus, _hEtaRatio);
      divide(_tmphistEtaChPlus, _tmphistEtaChMinus, _hEtaChRatio);
      divide(_tmphistRapPlus, _tmphistRapMinus, _hRapRatio);
      divide(_tmphistRapChPlus, _tmphistRapChMinus, _hRapChRatio);
      
      MSG_INFO(std::endl << "=======================================" << std::endl << \
      				"Number of photons: " << nphotons << std::endl << \
      				"min px, max px [GeV/c]: " << minPhtPx << " , " << maxPhtPx << std::endl << \
      				"min pz, max pz [GeV/c]: " << minPhtPz << " , " << maxPhtPz << std::endl << \
      				"min M, max M [MeV/c^2]: " << minPhtM << " , " << maxPhtM << std::endl << \
      				"=======================================" << std::endl << \
      				"Number of neutral pions: " << npions << std::endl << \
      				"min px, max px [GeV/c]: " << minPi0Px << " , " << maxPi0Px << std::endl << \
      				"min pz, max pz [GeV/c]: " << minPi0Pz << " , " << maxPi0Pz << std::endl << \
      				"min M, max M [MeV/c^2]: " << minPi0M << " , " << maxPi0M << std::endl << \
      				"=======================================" << std::endl << \
      				"Number of particles w/ rapidity=NaN: " << nerrnan << std::endl << \
      				"Number of particles w/ rapidity=Inf: " << nerrinf << std::endl << \
      				"=======================================" << std::endl);

      /*
      removeAnalysisObject(_tmphistEtaPlus);
      removeAnalysisObject(_tmphistEtaMinus);
      removeAnalysisObject(_tmphistEtaChPlus);
      removeAnalysisObject(_tmphistEtaChMinus);

      removeAnalysisObject(_tmphistRapPlus);
      removeAnalysisObject(_tmphistRapMinus);
      removeAnalysisObject(_tmphistRapChPlus);
      removeAnalysisObject(_tmphistRapChMinus);
      */
    }

    //@}


  private:

    /// Temporary histos used to calculate eta+/eta- ratio plot
    Histo1D _tmphistEtaPlus, _tmphistEtaMinus;
    Histo1D _tmphistEtaChPlus, _tmphistEtaChMinus;
    Histo1D _tmphistRapPlus, _tmphistRapMinus;
    Histo1D _tmphistRapChPlus, _tmphistRapChMinus;
    /// Counters for error suppression
    unsigned int nerrinf;
    unsigned int nerrnan;

    /// Temp variables:
    unsigned int nphotons;
    double maxPhtPx, minPhtPx, minPhtPz, maxPhtPz, minPhtM, maxPhtM;
    unsigned int npions;
    double maxPi0Px, minPi0Px, minPi0Pz, maxPi0Pz, minPi0M, maxPi0M;

    /// @name Histograms
    //@{
    Histo1DPtr _histMult, _histMultCh; 
    Profile1DPtr  _histEtaSumEt;
    Histo1DPtr _histEta, _histEtaCh;
    Histo1DPtr _histRapidity, _histRapidityCh;
    Histo1DPtr _histPt, _histPtCh;
    Histo1DPtr _histPx, _histPy;
    Histo1DPtr _histE, _histECh;
    Histo1DPtr _histPhi, _histPhiCh;
    Histo2DPtr _hPhtPx, _hPi0Px;
    Histo2DPtr _hPhtPz, _hPi0Pz;
    Scatter2DPtr _hEtaRatio, _hEtaChRatio, _hRapRatio, _hRapChRatio; 
    //@}

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_LHCb_GENERIC);

}
