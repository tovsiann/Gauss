/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: VariableLuminosity.h,v 1.3 2009-04-07 16:11:21 gcorti Exp $
#ifndef GENERATORS_VARIABLELUMINOSITY_H
#define GENERATORS_VARIABLELUMINOSITY_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"

#include "Generators/IPileUpTool.h"

// Forward declarations
class IRndmGenSvc ;
class ICounterLogFile ;

/** @class VariableLuminosity VariableLuminosity.h "VariableLuminosity.h"
 *
 *  Tool to compute variable number of pile up events depending on beam
 *  parameters, with time-dependant luminosity. Concrete implementation
 *  of a IPileUpTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-17
 */
class VariableLuminosity : public GaudiTool, virtual public IPileUpTool {
public:
  /// Standard constructor
  VariableLuminosity( const std::string& type, const std::string& name,
                      const IInterface* parent);

  virtual ~VariableLuminosity( ); ///< Destructor

  /// Initialize method
  StatusCode initialize( ) override;

  /// Finalize method
  StatusCode finalize( ) override;

  /** Compute number of interactions and returns luminosity
   *  Implements IPileUpTool::numberOfPileUp.
   *  The number of pileup interactions follows a Poisson law
   *  with mean equal to Luminosity * cross_section / crossing_frequency
   *  The Luminosity is exponentially decreasing with beam decay time.
   *  The mean luminosity is given in options so the maximum luminosity
   *  (at t=0) is computed using the fill duration.
   */
  unsigned int numberOfPileUp( ) override;

  /// Implements IPileUpTool::printPileUpCounters
  void printPileUpCounters( ) override;

private:
  /// Location where to store FSR counters (set by options)
  std::string  m_FSRName;

  std::string m_beamParameters ; ///< Location of beam parameters (set by options)

  double m_fillDuration ; ///< Fill duration (set by options)

  double m_beamDecayTime ; ///< Beam decay time (set by options)

  /// XML Log tool
  ICounterLogFile * m_xmlLogTool ;

  /// Counter of empty interactions
  int    m_numberOfZeroInteraction ;

  /// Counter of events (including empty interactions)
  int    m_nEvents ;

  /// Random number generator service
  IRndmGenSvc * m_randSvc ;

  /// Flat random number generator
  Rndm::Numbers m_flatGenerator ;
};
#endif // GENERATORS_VARIABLELUMINOSITY_H
