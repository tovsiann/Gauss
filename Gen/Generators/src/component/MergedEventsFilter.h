/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MergedEventsFilter.h,v 1.1 2008-05-06 08:27:55 gcorti Exp $
#ifndef GENERATORS_MERGEDEVENTSFILTER_H
#define GENERATORS_MERGEDEVENTSFILTER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

class IFullGenEventCutTool;

/** @class MergedEventsFilter MergedEventsFilter.h component/MergedEventsFilter.h
 *
 *
 *  @author Gloria CORTI
 *  @date   2008-04-30
 */
class MergedEventsFilter : public GaudiAlgorithm {
public:
  /// Standard constructor
  MergedEventsFilter( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~MergedEventsFilter( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:

  std::string  m_hepMCEventLocation ;    ///< Input TES for HepMC events
  std::string  m_genCollisionLocation ;  ///< Input TES for GenCollisions

  std::string           m_fullGenEventCutToolName; ///< Name of event cut tool
  IFullGenEventCutTool* m_fullGenEventCutTool;     ///< Pointer to event cut tool

};
#endif // GENERATORS_MERGEDEVENTSFILTER_H
