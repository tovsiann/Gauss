/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "BeamSpotMarkovChainSampleVertex.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LHCbAcceptance
//
// 2016-10-10 : Floris Keizer
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BeamSpotMarkovChainSampleVertex::
BeamSpotMarkovChainSampleVertex( const std::string& type,
                                 const std::string& name,
                                 const IInterface* parent )
  : GaudiTool ( type, name , parent )
{
  declareInterface< IVertexSmearingTool >( this ) ;
}

//=============================================================================
// Initialize 
//=============================================================================
StatusCode BeamSpotMarkovChainSampleVertex::initialize( )
{
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;

  // Random number generators to perturb the four-vector in the Markov chain
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = StatusCode( sc 
         && m_gaussDistX.initialize( randSvc , Rndm::Gauss( 0. , 0.010  ) ) //mm
         && m_gaussDistY.initialize( randSvc , Rndm::Gauss( 0. , 0.010  ) ) //mm
         && m_gaussDistZ.initialize( randSvc , Rndm::Gauss( 0. , 5.     ) ) //mm
         && m_gaussDistT.initialize( randSvc , Rndm::Gauss( 0. , 0.1    ) ) //ns
         && m_flatDist.initialize  ( randSvc , Rndm::Flat ( 0. , 1.     ) ) );
  if ( sc.isFailure() )
  { return Error( "Could not initialize random number generators" ); }

  release( randSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  return sc ;
}

//=============================================================================
// Function representing the product of two 4D Gaussian PDFs
//=============================================================================
double BeamSpotMarkovChainSampleVertex::gauss4D( LHCb::BeamParameters * beamp ,
                                                 const HepMC::FourVector & vec ) const
{
  // Import beam parameters
  const auto emittance = beamp -> emittance() ;
  const auto betastar  = beamp -> betaStar()  ;
  // RMS bunch length in mm:
  const auto sz        = beamp -> sigmaS()    ;
  const auto aX        = beamp -> verticalCrossingAngle();
  // aY is the half-crossing angle around the y-axis, in the horizontal (xz) plane:
  const auto aY        = beamp -> horizontalCrossingAngle();   

  const auto c  = Gaudi::Units::c_light ;
  const auto Pi = Gaudi::Units::pi;

  const auto sx = std::sqrt( emittance*betastar );
  const auto sy = sx       ;
  const auto x  = vec.x()  ;
  const auto y  = vec.y()  ;
  const auto z  = vec.z()  ;
  const auto t  = vec.t()  ;

  const auto sX2  = std::pow( sx , 2 ) ;
  const auto sY2  = std::pow( sy , 2 ) ;
  const auto sZ2  = std::pow( sz , 2 ) ;
  const auto x2   = std::pow( x  , 2 ) ;
  const auto y2   = std::pow( y  , 2 ) ;
  const auto z2   = std::pow( z  , 2 ) ;
  const auto t2   = std::pow( t  , 2 ) ;
  const auto c2   = std::pow( c  , 2 ) ;

  const auto c20  = std::cos( 2*aX          ) ; 
  const auto c40  = std::cos( 4*aX          ) ;
  const auto c01  = std::cos( aY            ) ;
  const auto c02  = std::cos( 2*aY          ) ;
  const auto c2p2 = std::cos( 2*(aX + aY)   ) ;
  const auto c2p1 = std::cos( 2*aX + aY     ) ;
  const auto c2m1 = std::cos( 2*aX - aY     ) ;
  const auto c2m2 = std::cos( 2*(aX - aY)   ) ;
  const auto c4p2 = std::cos( 2*(2*aX + aY) ) ;
  const auto c4p1 = std::cos( 4*aX + aY     ) ;
  const auto c4m1 = std::cos( 4*aX - aY     ) ;
  const auto c4m2 = std::cos( 4*aX - 2*aY   ) ;
  const auto s10  = std::sin( aX            ) ;
  const auto s30  = std::sin( 3*aX          ) ;
  const auto s1p2 = std::sin( aX + 2*aY     ) ;
  const auto s1p1 = std::sin( aX + aY       ) ;
  const auto s1m1 = std::sin( aX - aY       ) ;
  const auto s1m2 = std::sin( aX - 2*aY     ) ;
  const auto s3p2 = std::sin( 3*aX + 2*aY   ) ;
  const auto s3p1 = std::sin( 3*aX + aY     ) ;
  const auto s3m1 = std::sin( 3*aX - aY     ) ;
  const auto s3m2 = std::sin( 3*aX - 2*aY   ) ;
  
  // Return the function
  return std::exp( ( (-6*c2*t2)/sY2 - (18*c2*t2)/sZ2 - (16*x2)/sX2 - (8*x2)/sY2 - (8*x2)/sZ2 - (16*y2)/sY2 - (16*y2)/sZ2 - (16*z2)/sX2 - (8*z2)/sY2 - (8*z2)/sZ2 - (8*(c2*sY2*t2+(sY2-sZ2)*(x2-2*y2+z2))*c20)/(sY2*sZ2) - (6*c2*(sY2 - sZ2)*t2*c40)/(sY2*sZ2) + (c2*t2*c4m2)/sY2 - (c2*t2*c4m2)/sZ2 + (4*c2*t2*c2m2)/sZ2 - (4*x2*c2m2)/sY2 + (4*x2*c2m2)/sZ2 + (4*z2*c2m2)/sY2 - (4*z2*c2m2)/sZ2 + (16*x*y*c2m1)/sY2 - (16*x*y*c2m1)/sZ2 - (4*c2*t2*c4m1)/sY2 + (4*c2*t2*c4m1)/sZ2 + (8*c2*t2*c01)/sY2 - (8*c2*t2*c01)/sZ2 - (2*c2*t2*c02)/sY2 - (6*c2*t2*c02)/sZ2 - (16*x2*c02)/sX2 + (8*x2*c02)/sY2 + (8*x2*c02)/sZ2 + (16*z2*c02)/sX2 - (8*z2*c02)/sY2 - (8*z2*c02)/sZ2 + (4*c2*t2*c2p2)/sZ2 - (4*x2*c2p2)/sY2 + (4*x2*c2p2)/sZ2 + (4*z2*c2p2)/sY2 - (4*z2*c2p2)/sZ2 - (16*x*y*c2p1)/sY2 + (16*x*y*c2p1)/sZ2 + (c2*t2*c4p2)/sY2 - (c2*t2*c4p2)/sZ2 - (4*c2*t2*c4p1)/sY2 + (4*c2*t2*c4p1)/sZ2 + (16*c*t*y*s10)/sY2 - (16*c*t*y*s10)/sZ2 + (16*c*t*y*s30)/sY2 - (16*c*t*y*s30)/sZ2 - (4*c*t*x*s1m2)/sY2 + (4*c*t*x*s1m2)/sZ2 + (4*c*t*x*s3m2)/sY2 - (4*c*t*x*s3m2)/sZ2 + (8*c*t*x*s1m1)/sY2 + (24*c*t*x*s1m1)/sZ2 - (8*c*t*y*s1m1)/sY2 - (24*c*t*y*s1m1)/sZ2 - (8*c*t*x*s3m1)/sY2 + (8*c*t*x*s3m1)/sZ2 - (8*c*t*y*s3m1)/sY2 + (8*c*t*y*s3m1)/sZ2 - (8*c*t*x*s1p1)/sY2 - (24*c*t*x*s1p1)/sZ2 - (8*c*t*y*s1p1)/sY2 - (24*c*t*y*s1p1)/sZ2 + (8*c*t*x*s3p1)/sY2 - (8*c*t*x*s3p1)/sZ2 - (8*c*t*y*s3p1)/sY2 + (8*c*t*y*s3p1)/sZ2 + (4*c*t*x*s1p2)/sY2 - (4*c*t*x*s1p2)/sZ2 - (4*c*t*x*s3p2)/sY2 + (4*c*t*x*s3p2)/sZ2 )/32. ) / (8.*std::pow(Pi,3)*sX2*sY2*sZ2);
}

//=============================================================================
// Markov chain sampler : performed once per primary vertex
//=============================================================================
StatusCode BeamSpotMarkovChainSampleVertex::smearVertex( LHCb::HepMCEvent * theEvent )
{
 
  // Load the beam parameters 
  LHCb::BeamParameters * beamp = get< LHCb::BeamParameters >( m_beamParameters.value() ) ;
  if ( ! beamp ) Exception( "No beam parameters registered" ) ;

  // The sampled point, starting at the origin.
  HepMC::FourVector x( 0 , 0 , 0 , 0 );

  // Repeat for n-loops, provided a sampled point is obtained within the (x,y,z) limits
  unsigned int iLoop = 0; // sanity check to prevent infinite loops...
  bool OK = false;
  while ( iLoop++ < m_nMCSamples && !OK )
  {
    for ( unsigned int repeat = 0; repeat < m_nMCSamples; ++repeat ) 
    {
      // Compute the function for this point
      const auto f = gauss4D( beamp , x );
      
      // Smear the point (random walk).
      const HepMC::FourVector y( x.x() + m_gaussDistX(),
                                 x.y() + m_gaussDistY(),
                                 x.z() + m_gaussDistZ(),
                                 x.t() + m_gaussDistT() );    
      
      // Compute the function for the new point
      const auto g = gauss4D( beamp , y );    
      
      if ( f < g ) 
      {
        // The new point is better: always keep it
        x = y;
      }
      else
      {
	// The new point is worse: randomly keep the point a fraction of the times
	// depending on the probabilities f and g
        const auto ratio = ( fabs(f)>0 ? g/f : 0.0 );
        const auto r = m_flatDist( );
        if ( r < ratio ) { x = y; }	
      }
    }

    // Check if the spatial part of x is within the defined limits
    OK = ( ( fabs(x.x()) < ( m_xcut * beamp->sigmaX() ) ) &&
           ( fabs(x.y()) < ( m_ycut * beamp->sigmaY() ) ) &&
           ( fabs(x.z()) < ( m_zcut * beamp->sigmaZ() ) ) );

    // Reset and repeat
    if ( !OK ) { x = HepMC::FourVector(0,0,0,0); }

  }
  if ( !OK )
  {
    Warning( "Markov Chain sampling for PV (x,y,z,t) failed" ).ignore();
  }
  else
  {
    // Shift the sampled point to the average beam spot position
    x.setX( x.x() + beamp -> beamSpot().x() ); // Offset the centre of the beamspot
    x.setY( x.y() + beamp -> beamSpot().y() );
    x.setZ( x.z() + beamp -> beamSpot().z() );
    
    // update the values for all vertices belonging to the PV
    auto * pEvt = theEvent -> pGenEvt() ;
    if ( pEvt )
    {
      for ( auto vit = pEvt -> vertices_begin() ; vit != pEvt -> vertices_end() ; ++vit )
      {
        const auto pos = (*vit) -> position() ;
        (*vit) -> set_position( HepMC::FourVector( pos.x() + x.x() , 
                                                   pos.y() + x.y() , 
                                                   pos.z() + x.z() , 
                                                   pos.t() + x.t() ) ) ;
      }
    }    
  }  
  return StatusCode::SUCCESS;      
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( BeamSpotMarkovChainSampleVertex )
