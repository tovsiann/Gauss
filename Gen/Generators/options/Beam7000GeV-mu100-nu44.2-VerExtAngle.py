###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# File for setting hypotetical Beam conditions
# They are suitable for upgrade 2 studies:
#   Beam 7 TeV, beta* = 1.5m , emittance(normalized) ~ 2.5 micron
#   No spill-over
#
# Requires Gauss v45r1 or higher.
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu6.8-v45r1.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              Sim08-2011-Tags.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits
from math import sqrt
#--Set the L/nbb, total cross section and revolution frequency and configure
#  the pileup tool, a CrossingRate of 11.245 kilohertz is used internally
#  This is the L per bunch, corresponding to
#                           L = 1.5 x 10^34 cm-2 s-1 with 2400 colliding bunches
#  It correspond to nu(total) = 57 and we assume mu(visible)=0.699*nu
# the luminosity per bunch is corrected by a geometrical factor below 
luminosity = 1.5*(10**34)/(SystemOfUnits.cm2*SystemOfUnits.s)
nBunches   = 2400.

Gauss().TotalCrossSection = 102.5*SystemOfUnits.millibarn

#--Set the average position of the IP: assume a perfectly centered beam in LHCb
Gauss().InteractionPosition = [ 0.0, 0.0, 0.0 ]
# numbers taken from https://cds.cern.ch/record/2653011/files/LHCb-PUB-2019-001.pdf scenario 2 

Gauss().BunchRMS           = 76.1    * SystemOfUnits.mm
Gauss().BeamMomentum       =  7.0    * SystemOfUnits.TeV
Gauss().BeamHCrossingAngle =  0.135  * SystemOfUnits.mrad  # internal angle
Gauss().BeamVCrossingAngle = -0.160  * SystemOfUnits.mrad  # external angle
Gauss().BeamEmittance      =  0.0025 * SystemOfUnits.mm 
Gauss().BeamBetaStar       =  1.5    * SystemOfUnits.m

Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

sL              = Gauss().BunchRMS
gamma           = sqrt( 1 + (Gauss().BeamMomentum / (938.272 * SystemOfUnits.MeV)) **2 )
sT              = sqrt( Gauss().BeamEmittance * Gauss().BeamBetaStar / gamma )
t_eff           = Gauss().BeamHCrossingAngle **2 + Gauss().BeamVCrossingAngle **2
# the luminosity is reduced by a factor of ~ 1 / sqrt( 1 + (sL/sT)^2 * f^2 )
# where f is the (effective) crossing half angle, sL and sT the longitudinal and 
# transverse beam sizes
geom_reduction  = 1. / sqrt( 1 + ( sL/sT ) **2 * t_eff )

Gauss().Luminosity        = geom_reduction * luminosity / nBunches


