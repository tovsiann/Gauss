###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: Generators
################################################################################
gaudi_subdir(Generators v16r0)

gaudi_depends_on_subdirs(Event/GenEvent
                         GaudiAlg
                         Kernel/MCInterfaces
                         Kernel/LHCbKernel
                         Kernel/PartProp)

find_package(Boost COMPONENTS filesystem system)
find_package(ROOT)
find_package(HepMC)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

string(REPLACE "-pedantic" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-Wall" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-Wextra" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-Werror=return-type" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")

gaudi_add_library(GeneratorsLib
                  src/Lib/*.cpp src/Lib/*.F
                  PUBLIC_HEADERS Generators
                  INCLUDE_DIRS Kernel/MCInterfaces
                  LINK_LIBRARIES GenEvent GaudiAlgLib pythia6forgauss PartPropLib)

gaudi_add_module(Generators
                 src/component/*.cpp
                 INCLUDE_DIRS Boost Kernel/MCInterfaces
                 LINK_LIBRARIES Boost GenEvent GaudiAlgLib LHCbKernel pythia6forgauss GeneratorsLib)

gaudi_add_module(GeneratorTests
                 test/src/*.cpp
                 INCLUDE_DIRS Boost Kernel/MCInterfaces
                 LINK_LIBRARIES Boost GenEvent GaudiAlgLib LHCbKernel pythia6forgauss GeneratorsLib)


