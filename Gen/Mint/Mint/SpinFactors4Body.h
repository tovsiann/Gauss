/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SPINFACTORSSSS_FOURBODY_HH
#define SPINFACTORSSSS_FOURBODY_HH

#include "Mint/SpinFactors4Body_ScalarsAndVectors.h"
#include "Mint/SpinFactors4Body_Tensors.h"

#endif
//
