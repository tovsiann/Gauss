/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * MintGen.h
 *
 *  Created on: May 11, 2011
 *      Author: mcoombes
 *
 *      Purpose: Interface between Gauss model and MINT
 */

#ifndef GENERATOR_H_
#define GENERATOR_H_

//C++
#include <string>
// #include <iostream>
#include <vector>

//MINT
//#include "Mint/SignalGenerator.h"
#include "Mint/IDalitzEvent.h"
#include "Mint/IMintGen.h"

//ROOT
// #include "TVector3.h"
// #include "TRandom2.h"
// #include "TRandom3.h"
#include "TRandom.h"
// #include <ctime>

class TVector3;
class SignalGenerator;

namespace MINT {
	class MintGen : virtual public IMintGen{
	public:
		MintGen();
		~MintGen();

		void Initalize(const std::vector<int>& pat,
            TRandom* rnd=gRandom) override;

		void SetInputTextFile(std::string inputFile) override;

		// Decay Event in parent Rest Frame
		std::vector<std::vector<double> > DecayEventRFVec() override;

		std::vector<double> getDaughterMom(IDalitzEvent* , int );

		//DalitzEvent
		void SetDalitzEvent(IDalitzEvent*);

	private:
		std::string m_inputFileName;
		IDalitzEvent* m_dE;
		SignalGenerator* m_sg;
		bool m_swap;
	};
}

#endif /* MintGen_H_ */
