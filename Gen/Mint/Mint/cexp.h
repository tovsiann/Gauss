/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:56 GMT
#ifndef CEXP_THE_COMPLEX_EXP
#define CEXP_THE_COMPLEX_EXP

#include <complex>
#include <cmath>

namespace MINT{
template<typename T>
std::complex<T> cexp(std::complex<T> arg){
  std::complex<T> phase(cos(arg.imag()), sin(arg.imag()));
  return exp(arg.real())*phase;
}
}//namespace MINT

#endif
//
