/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:11 GMT
#include "Mint/SpinFactors.h"
//ClassImp(SF_DtoVP)
//ClassImp(SF_DtoTP)
//ClassImp(SF_DtoAP1_AtoVP2_VtoP3P4)
//ClassImp(SF_DtoAP1_AtoVP2_VtoP3P4)
//ClassImp(SF_DtoAP1_AtoSP2_StoP3P4)
//ClassImp(SF_DtoV1V2_V1toP1P2_V2toP3P4_S)
//ClassImp(SF_DtoVS_VtoP1P2_StoP3P4)
  //
