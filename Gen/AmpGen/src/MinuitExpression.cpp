/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AmpGen/MinuitExpression.h"

#include <stddef.h>

#include "AmpGen/Expression.h"
#include "AmpGen/ExpressionParser.h"
#include "AmpGen/MsgService.h"
#include "AmpGen/Utilities.h"

using namespace AmpGen;

MinuitExpression::MinuitExpression( const std::vector<std::string>& tokens, MinuitParameterSet* mps )
{
  setName( tokens[0] );
  m_expression = ExpressionParser::parse(tokens.begin() + 2 , tokens.end() , mps );
  m_isGood     = true;
  fix(); 
}
