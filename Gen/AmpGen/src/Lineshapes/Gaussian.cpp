/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <string>

#include "AmpGen/Expression.h"
#include "AmpGen/Factory.h"
#include "AmpGen/Lineshapes.h"

using namespace AmpGen;

DEFINE_LINESHAPE( Gaussian )
{
  Expression mu           = Parameter( lineshapeModifier + "_mean" );
  Expression sigma        = Parameter( lineshapeModifier + "_sigma" );
  const Expression d      = s - mu;
  return fcn::exp( -d * d / ( 2 * sigma * sigma ) ) ;
}
