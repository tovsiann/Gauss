/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <complex>
#include <string>
#include <vector>

#include "AmpGen/Factory.h"
#include "AmpGen/CoherentSum.h"
#include "AmpGen/IExtendLikelihood.h"
#include "AmpGen/Utilities.h"
#include "AmpGen/Types.h"

using namespace AmpGen;

double LASSO::getVal() const
{
  double sum( 0 );
  for ( unsigned int i = 0; i < m_pdf->size(); ++i ) {
    std::complex<double> c_i = (*m_pdf)[i].coefficient;
    sum += sqrt( std::norm(c_i) * m_pdf->norm(i, i).real() );
  }
  return m_lambda * sum;
}

void LASSO::configure( const std::string& configString, 
                       const CoherentSum& pdf,
                       const MinuitParameterSet& mps )
{
  m_pdf       = &pdf;
  auto tokens = split( configString, ' ' );
  m_lambda    = stod( tokens[1] );
}

REGISTER( IExtendLikelihood, LASSO );
