/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef AMPGEN_SIMPLIFY_H
#define AMPGEN_SIMPLIFY_H 1
#include <complex>
#include <string>
#include <utility>
#include <vector>

#include "AmpGen/Expression.h"

namespace AmpGen { 
  class NormalOrderedExpression { 
    public:
      struct Term {
        std::complex<double>                              m_prefactor; 
        std::vector<std::pair<Expression,std::string>>    m_terms;
        Expression                                        m_divisor; 
        std::string                                       m_expressionAsString; 
        bool                                              m_markForRemoval;
        void addExpression( const Expression& expression);
        Term( const Expression& expression ) ;
        operator Expression() ;
      };
      void addTerm( const Expression& expression );
      NormalOrderedExpression( const Expression& expression, const bool& expandSubTrees=false );
      void groupExpressions();

      operator Expression();
      std::vector<Expression> ExpandBrackets( const Expression& expression );
      std::vector<Term> terms() const { return m_terms ; }
    private:
      std::vector<Term> m_terms;
      bool m_expandSubTrees; 
  };
  Expression Simplify(const Expression& expression );
}

#endif
