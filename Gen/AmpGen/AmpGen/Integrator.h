/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef AMPGEN_INTEGRATOR_H
#define AMPGEN_INTEGRATOR_H

#include "AmpGen/Types.h"
#include "AmpGen/EventList.h"
#include <array>
#include <complex>

/*
 *  Calculates Bilinears A_i A_j^* integrated over the phase-space.
 *  Integrates in blocks of (i,j) such that integrals can be queued and evaluated in blocks
 *  to optimise cache throughput.
 */

namespace AmpGen
{
  class Bilinears 
  {
    private: 
      size_t rows;
      size_t cols;
      std::vector<complex_t> norms;
      std::vector<bool> markAsZero; 
      std::vector<bool> calculate;   
    public:
      Bilinears( const size_t& r = 0, const size_t& c = 0 );
      complex_t get(const size_t& x, const size_t& y) const;
      template <class T>
      complex_t get(const size_t& x, const size_t& y, T* integ = nullptr, const size_t& kx=0, const size_t& ky=0){
        if( integ != nullptr ) integ->queueIntegral(kx, ky, &norms[x*cols+y]);
        /// will return the wrong answer for now, but queues for later.. 
        return norms[x*cols+y];
      }
      void      set(const size_t& x, const size_t& y,  const complex_t& f );
      void  setZero(const size_t& x, const size_t& y);
      void resetCalculateFlags();
      complex_t& operator()( const size_t& x, const size_t& y );
      bool   isZero(const size_t& x, const size_t& y);
      bool workToDo(const size_t& x, const size_t& y) const;
      void   resize(const size_t& r, const size_t& c = 1 );
  };

  template <class TYPE = complex_t>
    struct Integral {
      typedef std::function<void(TYPE)> TransferFCN;
      size_t i = {0};
      size_t j = {0};
      TransferFCN transfer;
      Integral() = default; 
      Integral(const size_t& i, const size_t& j, TransferFCN t) : i(i), j(j), transfer(t) {}
    };

  template <size_t NROLL = 10>
    class Integrator
    {
      private:
        typedef const complex_t& arg;
        typedef std::function<void(arg)> TransferFCN;
        size_t                           m_counter = {0};
        std::array<Integral<arg>, NROLL> m_integrals;
        EventList*                       m_events  = {nullptr};
        void calculate()
        {
          integrateBlock();
          m_counter = 0;
        }
        void integrateBlock()
        {
          real_t re[NROLL] = {0};
          real_t im[NROLL] = {0};
          #if __USE_OPENMP__ && __cplusplus >= 201402L
          #pragma omp parallel for reduction(+: re, im)
          #endif
          for ( size_t i = 0; i < m_events->size(); ++i ) {
            auto& evt = ( *m_events )[i];
            real_t w  = evt.weight() / evt.genPdf();
            for ( size_t roll = 0; roll < NROLL; ++roll ) {
              auto c = evt.getCache(m_integrals[roll].i) * std::conj(evt.getCache(m_integrals[roll].j));
              re[roll] += w * std::real(c);
              im[roll] += w * std::imag(c);
            }
          }
          real_t nv = m_events->norm();
          for ( size_t j = 0; j < m_counter; ++j )
            m_integrals[j].transfer( complex_t( re[j], im[j] ) / nv );
        }
      public:
        Integrator( EventList* events = nullptr ) : m_events( events ){}
        
        double sampleNorm() { return m_events->norm(); } 
        bool isReady() const { return m_events != nullptr ; }
        const EventList& events() const { return *m_events ; } 
        template <class T1, class T2>
          void addIntegral( const T1& f1, const T2& f2, const TransferFCN& tFunc )
          {
            addIntegralKeyed( m_events->getCacheIndex(f1), m_events->getCacheIndex(f2), tFunc );
          }
        void queueIntegral(const size_t& i, const size_t& j, complex_t* result){
          addIntegralKeyed(i, j, [result](arg& val){ *result = val ; } ); 
        }
        void queueIntegral(const size_t& c1, 
                           const size_t& c2, 
                           const size_t& i, 
                           const size_t& j, 
                           Bilinears* out, 
                           const bool& sim = true )
        {
          if( out->workToDo(i,j) ){
            if( sim ) 
              addIntegralKeyed( c1, c2, [out,i,j]( arg& val ){ 
                out->set(i,j,val);
                if( i != j ) out->set(j,i, std::conj(val) ); } );
            else 
              addIntegralKeyed( c1, c2, [out,i,j]( arg& val ){ out->set(i,j,val); } );
          }
        }
        void addIntegralKeyed( const size_t& c1, const size_t& c2, const TransferFCN& tFunc )
        {
          m_integrals[m_counter++] = Integral<arg>(c1,c2,tFunc);
          if ( m_counter == NROLL ) calculate();
        }

        void flush()
        {
          if ( m_counter == 0 ) return;
          calculate();
        }
        template <class EXPRESSION>
          void prepareExpression( const EXPRESSION& expression, const size_t& size_of = 0 )
          {
            if( m_events == nullptr ) return; 
            auto index = m_events->registerExpression( expression , size_of );
            m_events->updateCache( expression, index );
          }
    };
} // namespace AmpGen
#endif
