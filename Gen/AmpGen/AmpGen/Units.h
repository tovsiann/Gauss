/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef AMPGEN_UNITS_H
#define AMPGEN_UNITS_H 1

namespace AmpGen {
  static const double TeV = 1000;
  static const double GeV = 1;
  static const double MeV = 0.001;
  static const double KeV = 0.001*0.001;
}
#endif
