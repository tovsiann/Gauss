/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef AMPGEN_SIMPDF_H
#define AMPGEN_SIMPDF_H

#include <tuple>

namespace AmpGen
{
  class SimFit
  {
    std::vector<std::function<double( void )>> m_pdfs;

  public:
    SimFit() {}
    double getVal()
    {
      double LL = 0;
      for ( auto& pdf : m_pdfs ) LL += pdf();
      return LL;
    }

    template <class PDF>
    void add( PDF& pdf )
    {
      INFO( "Adding " << &pdf << " to sim fit" );
      m_pdfs.emplace_back( [&pdf]() -> double { return pdf.getVal(); } );
    }
  };
} // namespace AmpGen

#endif
