/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef EVTMODELREGEXTRAS_H_
#define EVTMODELREGEXTRAS_H_

#include <memory>
#include "EvtGenModels/EvtModelReg.hh"
/** 
 * Provides a list of external decay models to
 * use with EvtGen.
*/
typedef std::list<EvtDecayBase*> EvtModelList;
class EvtModelRegExtras
{
public:
	static std::unique_ptr<const EvtModelList> getModels();
};

#endif /*EVTMODELREGEXTRAS_H_*/
