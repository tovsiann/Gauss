//--------------------------------------------------------------------------
//
// Copyright Information: See EvtGen/COPYRIGHT
//
// Module: EvtBnoCBpto3hpi0
//
// Description: Model for generating flat phase space events in the charmless
//              VV region. Necessary due to limitations with generator-level
//              cuts in LoKi; inability to handle Bose-symmetric final states
//              with PHOTOS
//
// Modification history:
//
//    Jeremy Dalseno     Mar 2020     Module created
//
//------------------------------------------------------------------------

#include "EvtGenBase/EvtParticle.hh"
#include "EvtGenBase/EvtPDL.hh"

#include "EvtGenModels/EvtBnoCBpto3hpi0.hh"

// Constructor defining parameters from PDG 2018
EvtBnoCBpto3hpi0::EvtBnoCBpto3hpi0() :
  _bose_symm(false),
  _m2hp_ll(0.0),
  _m2hp_ul(1.86965),//D+
  _m2h0_ll(0.0),
  _m2h0_ul(1.86483),//D0
  _I(EvtComplex(1.0, 0.0))
{
}

void EvtBnoCBpto3hpi0::init()
{
  // Check there are 0 or 4 arguments
  checkNArg(0,4);
  checkNDaug(4);

  // Check initial state particles
  if (getParentId() != EvtPDL::getId("B+") && getParentId() != EvtPDL::getId("B-") &&
      getParentId() != EvtPDL::getId("B_c+") && getParentId() != EvtPDL::getId("B_c-")) {
    EvtGenReport(EVTGEN_ERROR,"EvtGenExtras") << getModelName()
					      << " generator expects initial state to be a charged Bu or Bc"
					      << std::endl;
    ::abort();
  }

  // Check final state particles
  for (unsigned int i = 0; i < static_cast<unsigned int>(getNDaug())-1; ++i) {
    if (getDaug(i) != EvtPDL::getId("pi+") && getDaug(i) != EvtPDL::getId("pi-") &&
	getDaug(i) != EvtPDL::getId("K+") && getDaug(i) != EvtPDL::getId("K-")) {
      EvtGenReport(EVTGEN_ERROR,"EvtGenExtras") << getModelName()
						<< " generator expects final state particle " << i
						<< " to be either a h+ or h-" << std::endl;
      ::abort();
    }
  }

  if (getDaug(3) != EvtPDL::getId("pi0")) {
    EvtGenReport(EVTGEN_ERROR,"EvtGenExtras") << getModelName()
					      << " generator expects 4th final state particle to be a pi0"
					      << std::endl;
    ::abort();
  }

  // Enforce charge ordering
  if (EvtPDL::chg3(getDaug(0)) != EvtPDL::chg3(getDaug(2))) {
    EvtGenReport(EVTGEN_ERROR,"EvtGenExtras") << getModelName()
					      << " generator expects final state particles 0 and 2 to have the same charge"
					      << std::endl;
    ::abort();
  }

  // Check for Bose symmetric final state
  if (getDaug(0) == getDaug(2)) {
    _bose_symm = true;
  }

  // Assign arguments
  if (getNArg() == 4) {

    // Lower and upper (2h)+ invariant mass limits
    _m2hp_ll = getArg(0);
    _m2hp_ul = getArg(1);

    // Lower and upper (2h)0 invariant mass limits
    _m2h0_ll = getArg(2);
    _m2h0_ul = getArg(3);

  }

}

void EvtBnoCBpto3hpi0::decay(EvtParticle* p)
{
  // Generate 4-vectors
  p->initializePhaseSpace( getNDaug(), getDaugs() );

  // Apply charmless selection
  const EvtVector4R p1 = p->getDaug(0)->getP4();
  const EvtVector4R p2 = p->getDaug(1)->getP4();
  const EvtVector4R p3 = p->getDaug(2)->getP4();
  const EvtVector4R p4 = p->getDaug(3)->getP4();

  const double m12 = (p1+p2).mass();
  const double m34 = (p3+p4).mass();
  const double m14 = (p1+p4).mass();
  const double m23 = (p2+p3).mass();

  EvtComplex amp(0.0,0.0);

  if (_bose_symm == true) {
    if ( ((m12 > _m2h0_ll && m12 < _m2h0_ul) &&
	  (m34 > _m2hp_ll && m34 < _m2hp_ul)) ||
	 ((m14 > _m2hp_ll && m14 < _m2hp_ul) &&
	  (m23 > _m2h0_ll && m23 < _m2h0_ul)) ) {

      amp = _I;

    }

  } else {
    if ((m12 > _m2h0_ll && m12 < _m2h0_ul) &&
	(m34 > _m2hp_ll && m34 < _m2hp_ul)) {

      amp = _I;

    }

  }

  vertex(amp);

}
