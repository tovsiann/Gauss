/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Rconst.h,v 1.2 2006-05-03 08:24:32 robbep Exp $

#ifndef LBBCVEGPY_RCONST_H
#define LBBCVEGPY_RCONST_H 1

#ifdef WIN32
extern "C" {
  void* __stdcall RCONST_ADDRESS(void) ;
}
#else
extern "C" {
  void* rconst_address__(void) ;
}
#endif

class Rconst {
public:
  Rconst();
  ~Rconst();
  double& pi();
  inline void init(); // inlined for speed of access (small function)
  // return common array lengths
private:
  struct RCONST;
  friend struct RCONST;
  
  struct RCONST {
    double pi;
  };
  int m_dummy;
  double m_realdummy;
  static RCONST* s_rconst;
};

// Inline implementations for Rconst 
// initialise pointer
#ifdef WIN32
void Rconst::init(void) {
  if ( 0 == s_rconst ) s_rconst = static_cast<RCONST*>(RCONST_ADDRESS());
}
#else
void Rconst::init(void) {
  if ( 0 == s_rconst ) s_rconst = static_cast<RCONST*>(rconst_address__());
}
#endif
#endif // LBBCVEGPY_RCONST_H
 
