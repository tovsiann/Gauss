2018-08-31 Gauss v53r0
===

This version uses LHCb **v50r1**, Geant4 v104r2, Gaudi **v30r3** and LCG 93 with
 HepMC 2.06.09 and Root 6.12.06

The generators used via LCG_93/MCGenerators) are
 pythia8 235 (with LHCb old Tune1), lhapdf 6.1.6.cxxstd, photos++ 3.56, 
 tauola++ 1.1.6b.lhcb, pythia 6.427.2,
 hijing 1.383bs.2, crmc 1.5.6 (epos), alpgen 2.1.4, powhegbox r3043.lhcb, 
 herwig++ 2.7.1, thepeg 2.1.1, 
 rivet 2.6.0, yoda 1.6.7.
 startlight r300  
and the internal implementation of:
 EvtGen with EvtGenExtras, AmpGen, Mint,
 BcVegPy and GenXicc, SuperChic2, LPair, MIB

The data packages specific for the simulation are 
Geant4Files v104r* , GDMLData v1r* , XmlVis v2r* , DecFiles v30r* , 
LHAPDFSets v2r* , BcVegPyData v2r* , GenXiccData v3r* , PGunsData v1r* , 
MIBData v3r*


<p>
This version is released on `master`. 
<p>
It is to be used for productions referred to as **Sim10Dev** and is for validation, checks on tuning, 
setting up and cross check in view of Sim10.

### New features

#### Detector Simulation

- Change dddb tags to pick up new one with new M1 geometry included, !327 (@gcorti) [LHCBGAUSS-1224]  
  Details in LHCBGAUSS-1224


### Enhancements

#### Generators

- Gauss:  the variant of MR 318 for master branch, !330 (@ibelyaev) [LHCBGAUSS-1437]  
  This is a variant of !318   for `master` branch and replaces !322 that had merge conflicts.      
  Corresponds to merge request lhcb/Gauss!318 into 'Sim09' and lhcb/Gauss!321 into 'Sim09-upgrade'  
  Details at LHCBGAUSS-1437

#### Detector Simulation

- Check DDDB tag for custom user git branch name, !319 (@jadevrie)   
  added check for custom DDDB tags in VELO geometry date test. This prevents a crash if users run Gauss using a custom git DDDB branch without a date in the name.


### Bug fixes

- Fix bug introduced when adapting to removal of execute from LbAppInit, i.e. increase number of events processed, !334 (@gcorti)   
  Add missing call to increase number of events processed that was left to zero and hence the event number.  
  This bug was introduced when adapting to removal of `execute` from `LbAppInit` as a new function now needs to be called, `increaseEventCounter()`.  
  This is also needed to properly set the random number seed otherwise it is the same for all events within the same `Run`  
  This should solve all the qmtest failing because of missing the correct number of events processed, e.g. https://lhcb-nightlies.cern.ch/logs/tests/nightly/lhcb-gauss-dev/1746/x86_64-slc6-gcc7-opt/Gauss/  
  due to the current validator:  
  ```  
  expected_string = "Application Manager Terminated successfully"  
  if stdout.find(expected_string) == -1:  
  	causes.append('missing string')  
  	result['GaudiTest.expected_string'] = result.Quote(expected_string)  
  else:  
  	expected_string2 = "100 events processed"  
  	if stdout.find(expected_string2) == -1:  
  		causes.append('missing string')  
  		result['GaudiTest.expected_string'] = result.Quote(expected_string2)  
  ```


### Code modernisations and cleanups

- Fixed many compilation warnings, !333 (@sponce)   
  

- Fix fake build errors with Gaudi v30r3, !332 (@gcorti)   
  Fix fake build errors reported for Gauss in https://lhcb-nightlies.cern.ch/logs/build/nightly/lhcb-gauss-dev/1740/x86_64-slc6-gcc7-opt/Gauss/,   
  From @cattanem: They are false positives and are trivial to fix. e.g.:  
  ```C++  
  Sim/GaussMonitor/src/CheckLifeTime.cpp:113:60: warning: ignoring return value of 'StatusCode GaudiCommon<PBASE>::Error(const string&, StatusCode, size_t) const [with PBASE = Algorithm; std::__cxx11::string = std::__cxx11::basic_string<char>; size_t = long unsigned int]', declared with attribute nodiscard [-Wunused-result]  
       if ( 0 == pp ) { Error ( "Invalid particle: " + (*ip)) ; continue ; }  
  ```  
  Change the above line to  
  ```C++  
       if ( 0 == pp ) { Error ( "Invalid particle: " + (*ip)).ignore() ; continue ; }  
  ```  
  Similarly you could get rid of a lot  of warnings:  
  ```C++  
  Sim/GaussMonitor/src/CheckLifeTime.cpp:115:60: warning: ignoring return value of 'StatusCode GaudiCommon<PBASE>::Warning(const string&, StatusCode, size_t) const [with PBASE = Algorithm; std::__cxx11::string = std::__cxx11::basic_string<char>; size_t = long unsigned int]', declared with attribute nodiscard [-Wunused-result]  
       { Warning ( "No '" + (*ip) + "' Particles are found!") ; continue ; }  
  ```  
  Change it to   
  ```C++  
       { Warning ( "No '" + (*ip) + "' Particles are found!‚Äù).ignore() ; continue ; }  
  ```  
  This MR addresses the Errors and the Warnings in the same package where the Errors were present, i.e. `GaussAlgs`, `GaussMonitor`, `GaussRICH` and `GaussCherenkov`  
    

- Fixes for new Gaudi, !324 (@gcorti)   
  Use `DECLARE_COMPONENT` for algorithm and tool factory in files that were committed after the fixes by @clemenci in MR !265      
  Replace `DECLARE_CONVERTER_FACTORY` with `DECLARE_CONVERTER` for compatibility with latest Gaudi (it is likely not going to compile with Gaudi-v29patches)  
  

- Various fixes, !265 (@clemenci)   
  - hide warning from external projects  
  - fix missing `override`  
  - add missing dependency on Vc  
  - fix misc warnings  
  - follow up gaudi/Gaudi!514  
  - prepare for gaudi/Gaudi!604  
  *Note* requires ROOT >= 6.08 (6473efee) and Geant4 >= 10.0 (6a2bc275)

- Removed use of deprecated factories, !225 (@clemenci)   
  - replaced `DECLARE_*_FACTORY` with `DECLARE_COMPONENT`  
  - made constructors public  
  - removed some unnecessary _disabling_ of copy constructors  
  - removed some trivial destructors  
  - removed use of deprecated headers  
  See gaudi/Gaudi!420


### Monitoring changes and Changes to tests

- Update reference tests, !336 (@gcorti)   
  Update reference tests. Some are still broken, but these are the essential minimal test that must work.  
  At some point they will require cleanup...

- Update testGauss-config-defaults.py to print list of sensdet to be simulated, !335 (@gcorti)   
  Due to the change of default geometry transformation service from `GiGaInputStream` and geometry converters to the `GaussGeo` service the list of sensitive detectors to be simulated was no longer printed in the configuration tests.  
  Modify the setting of the options to have them printed out again.  
  Once the simulation is validated for all data taking periods in production the old conversion mechanism will be removed.

- Add datatypes 2017 and 2018 to list of those for Run2, !328 (@gcorti)   
  Port merge of branch 'cherry-pick-47e585a7' into 'master'  
  See merge request lhcb/Gauss!326 into 'Sim09-upgrade', also corresponding to MR lhcb/Gauss!320  
  (cherry picked from commit 2857a81ba560194760b81a306dd2c4a914e5f168)  
  b19db545 Merge branch 'dataTypes' into 'Sim09'    
  891bb7f3 Update Gauss-2017.py to pick the post Sim09 tag (for RICH speed-up)

