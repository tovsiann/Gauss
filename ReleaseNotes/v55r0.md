2021-03-25 Gauss v55r0
===

This version uses 
Run2Support [v1r0](../../../../Run2Support/-/tags/v1r0),
LHCb [v52r0](../../../../LHCb/-/tags/v52r0),
Gaudi [v35r2](../../../../Gaudi/-/tags/v35r2), 
Geant4 [v106r2p3](../../../../Geant4/-/tags/v106r2p3) and
LCG [97a_LHCB_5](http://lcginfo.cern.ch/release/97a_LHCB_5/) with ROOT 6.20.06.

The following generators are used from LCG [97a_LHCB_5](http://lcginfo.cern.ch/release/97a_LHCB_5/):
- pythia8   244.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- herwig++  7.2.1,      thepeg 2.2.1
- :warning: crmc      *1.8.0.lhcb*,       starlight r300
- rivet     2.7.2b,     yoda   1.8.2   

while the followings are built privately:
- EvtGen with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB
- Hijing (1.383bs.2)


This version is released on `master` branch.
Built relative to Gauss [v54r5](../-/tags/v54r5), with the following changes:

### New features ~"new feature"

- ~Generators | Move Phys/LoKiGen from LHCb to Gauss, !704 (@rmatev)
- ~Generators | Add selection cut for Diboson event to master, !689 (@gcorti)
- ~Generators | Modifying BcVegPy and GenCuts to add production of excited Bc states, !677 (@gcorti from original by @lan) [LHCBGAUSS-1566]
- ~Build | Add Run2Support dependency, !702 (@adavis)


### Fixes ~"bug fix" ~workaround

- ~"Detector simulation" | Fix in muon low energy options, !703 (@gcorti)
- ~Generators ~IFT | Adapt to change in initialisation for CRMC 1.8.0, !724 (@gcorti) [LHCBGAUSS-2179]
- ~Build | Fix linking with CLHEP libraries, !692 (@gcorti)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Update DDDB tags, needed for compatibility with DecFiles >= v31r5, !712 (@gcorti) [lhcb-conddb/DDDB#8]
- ~Configuration | Attempt to make Gauss compatible with python3, !607 (@kreps)
- ~Configuration ~Generators | Use LHCB_5 layer with new generators, !713 (@gcorti) [LHCBGAUSS-2179,LHCBGAUSS-2216]
- ~Generators | Fixes for lhcb-lcg-dev3 and lhcb-lcg-dev4, !678 (@kreps)
- ~Build | Prefer GaudiProjectConfig.cmake from LbDevTools to that in Gaudi, !691 (@clemenci)
- Update reference for v55r0 release, !722 (@gcorti)
- Update reference for gauss-config-2016-epos-phe, !705 (@rmatev)
- Fix most warnings, !698 (@rmatev)
- Remove unneeded cleanup from test output preprocessing, !690 (@kreps)


### Documentation ~Documentation

- Documentation and dependencies for v55r0 release, !726 (@gcorti)

