2020-06-26 Gauss v54r3
===

This version uses LHCb v51r0, Gaudi v33r0, Geant4 v106r2 and LCG_96b with ROOT 6.18.04.

The following generators are used from LCG_96b:
- pythia8   240.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- herwig++  2.7.1,      thepeg 2.1.5
- crmc      1.5.6,      hijing 1.383bs.2, starlight r300
- rivet     2.7.2b,     yoda   1.7.7   

while the followings are built privately:
- EvtGen with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB


This version is released on `master` branch.
Built relative to Gauss v54r2, with the following changes:

### New features ~"new feature"

- ~Configuration ~"Detector simulation" ~Geant4 | Change default EM physics list to Opt2 for Sim10, !615 (@gcorti) [LHCBGAUSS-2026,LHCBGAUSS-2046]


### Fixes ~"bug fix" ~workaround

- ~"Detector simulation" ~Generators ~IFT | Workaround to avoid passing large energy heavy ions to Geant4, !614 (@kreps)


### Enhancements ~enhancement

- ~"Fast Simulations" | Add PG PrimaryVertex Configuration and Tests for sim10, !602 (@adavis)
- ~Generators | Daughters in LHCb and cuts for D(s) for master, !608 (@gcorti)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration ~"Detector simulation" | Update tags for upgrade exemples to use the latest, !620 (@gcorti)
- ~Configuration ~"Detector simulation" ~Muon ~Simulation | Remove choice of hadronic physics list from muon low energy options, !619 (@gcorti)
- ~Configuration ~Simulation ~Generators | Update dddb and simcond tags for Sim10, !592 (@gcorti) [lhcb-conddb/DDDB#3,lhcb-conddb/SIMCOND#6,LHCBGAUSS-1224]
- ~"Detector simulation" ~Monitoring | MonitorTiming update, !590 (@dpopov)
- ~RICH | GaussCherenkov minor cleanup with recent LHCb, !594 (@seaso) [LHCBGAUSS-2024]
- ~Simulation ~"Fast Simulations" ~Generators | Update test refs due to expected changes from MR, !618 (@gcorti)
- Ignore unchecked status codes, !576 (@clemenci)


### Documentation ~Documentation

- Documentation and dependecies for v54r3 release, !622 (@gcorti)


### Other

- ~Generators ~Pythia8 | Change version of LHAPDFset with new convention, !621 (@gcorti)
