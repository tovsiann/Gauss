2021-06-18 Gauss v55r1
===

This version uses 
Run2Support [v1r1](../../../../Run2Support/-/tags/v1r1),
LHCb [v52r1](../../../../LHCb/-/tags/v52r1),
Gaudi [v35r4](../../../../../gaudi/Gaudi/-/tags/v35r4) and
Geant4 [v106r2p4](../../../../Geant4/-/tags/v106r2p4),
LCG [100_LHCB_5](http://lcginfo.cern.ch/release/100_LHCB_5/) with ROOT 6.24.00.

The following generators are used from LCG [100_LHCB_5](http://lcginfo.cern.ch/release/100_LHCB_5/):
- pythia8   244.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- herwig++  7.2.1,      thepeg 2.2.1
- crmc      1.8.0.lhcb, starlight r300
- rivet     2.7.2b,     yoda   1.8.2   

while the followings are built privately:
- EvtGen with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB
- Hijing (1.383bs.2)


This version is released on `master` branch.
Built relative to Gauss [v55r0](../-/tags/v55r0), with the following changes:


### Fixes ~"bug fix" ~workaround

- ~Generators | Fix typo in setting tuning for Inclusive production tool, !728 (@kreps)


### Enhancements ~enhancement

- ~Build | Fix missing linker dependencies (for ubsan build), !707 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Updated references for tests to prepare v55r1 release, !743 (@gcorti)


### Documentation ~Documentation


### Other

- ~Configuration | Follow LHCb!2974, !720 (@apearce)
